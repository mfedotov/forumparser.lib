//
//Copyright (C) 2016 Mikhail Fedotov <anyremote@mail.ru>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA. 
//

package ar.forum;

import java.util.ArrayList;
import java.util.Vector;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.text.util.Linkify;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import ar.forum.utils.Author;
import ar.forum.utils.ForumThreadData;
import ar.forum.utils.Prefs;
import ar.forum.utils.ResourceProvider;

public class ThreadAdapter extends BaseAdapter<ForumThreadData> implements OnClickListener {
    
    //String m_colorize;
    int    m_fg;
    
    public ThreadAdapter(Context context, int textViewResourceId, ArrayList<ForumThreadData> items) {
        super(context, textViewResourceId, items);
        
        this.context = context;
        
        //int bg = this.context.getResources().getColor(DataCollector.colorBkgr());
        m_fg = this.context.getResources().getColor(DataCollector.colorText());
        
        //m_colorize = "<body style=\"background-color:#"+ Integer.toHexString(bg).substring(2) +
        //                         ";color:#" + Integer.toHexString(m_fg).substring(2) + "\">\n";
    }
     
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final View v;

        //Log.i("ThreadAdapter","getView "+position);
        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(DataCollector.resourceId(ResourceProvider.THREAD_LIST_ITEM_EX), null);
        } else {
            v = convertView;
        }
        
        int margin = DataCollector.screenWidth() / 15;
        
        LinearLayout row = (LinearLayout) v.findViewById(DataCollector.resourceId(ResourceProvider.THREAD_ROW));
        WebView page = (WebView)  v.findViewById(DataCollector.resourceId(ResourceProvider.THREAD_HTML));
        
        WebSettings webSettings = page.getSettings();
        
        webSettings.setJavaScriptEnabled(false); 
        webSettings.setJavaScriptCanOpenWindowsAutomatically(false);
        webSettings.setSupportMultipleWindows(false);
        webSettings.setSupportZoom(false);
        webSettings.setBuiltInZoomControls(false);
        webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);

        page.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.i("ThreadAdapter", "shouldOverrideUrlLoading " + url);
                if (url.startsWith("http")) {
                    DataCollector.sendGlobal(BaseProvider.SHOW_WEB, url);
                }
                return true;
            }
        });


        boolean resetSize = false;
        TextView txt = (TextView) v.findViewById(DataCollector.resourceId(ResourceProvider.THREAD_TITLE));
        txt.setTextColor(m_fg);
        
        int ts = (int) txt.getTextSize();
        int textSize = DataCollector.prefs().textSize(Prefs.POST_TEXT);
        if (textSize < 0) { 
            DataCollector.prefs().saveTextSize(null,Prefs.POST_TEXT,ts);
            DataCollector.sendGlobal(BaseProvider.SETUP_POST_SIZE, textSize);  // will save text size value again
        } else if (textSize != ts) {
            txt.setTextSize(textSize);
            resetSize = true;
        }

        webSettings.setDefaultFontSize(textSize);
       
        TextView aut = (TextView) v.findViewById(DataCollector.resourceId(ResourceProvider.USER));
        aut.setOnClickListener(this);
        
        TextView dat = (TextView) v.findViewById(DataCollector.resourceId(ResourceProvider.DATE));
        TextView ans = (TextView) v.findViewById(DataCollector.resourceId(ResourceProvider.ANSWERS));

        aut.setTextColor(this.context.getResources().getColor(DataCollector.colorTextGrayed()));
        dat.setTextColor(this.context.getResources().getColor(DataCollector.colorTextGrayed()));
        ans.setVisibility(TextView.GONE);
        
        if (resetSize) {
            aut.setTextSize(textSize-1);  // a bit smaller than post text
            dat.setTextSize(textSize-1);
        }
         
        synchronized (items) {
            ForumThreadData item = items.get(position);
            if (item != null) {

                if (item.level > 0) {
                    //Log.i("ThreadAdapter","level "+item.level+" "+item.description);
                    row.setPadding(item.level * margin,
                            row.getPaddingTop(),
                            row.getPaddingRight(),
                            row.getPaddingBottom());
                } else {
                    row.setPadding(row.getPaddingRight(),
                            row.getPaddingTop(),
                            row.getPaddingRight(),
                            row.getPaddingBottom());
                }
                //Log.i("ThreadAdapter", "HTML=\n"+Html.fromHtml(item.html));

                if (item.html.isEmpty() /*item.level > 0*/) {
                    page.setVisibility(View.GONE);
                } else {
                    page.setVisibility(View.VISIBLE);
                    //Log.i("ThreadAdapter", "HTML\n<style>img{display: inline; height: auto; max-width: 100%;}\n "+
                    //                       DataCollector.provider().threadCSS()+"</style>\n" +
                    //                       m_colorize + item.html + "\n</body>");

                    //page.loadData("<style>img{display: inline; height: auto; max-width: 100%;}</style>\n"
                    //                + m_colorize + item.html + "\n</body>",
                    //              "text/html; charset=" + DataCollector.provider().encoding(), null);

                    page.loadDataWithBaseURL(null, "<style>img{display: inline; height: auto; max-width: 100%;} "+
                                                   DataCollector.provider().threadCSS()+
                                                   "</style>\n" +
                                                   /*m_colorize*/"<body> " + item.html + "\n</body>",
                                             "text/html", DataCollector.provider().encoding(),
                                             null);
                    int bg = this.context.getResources().getColor(DataCollector.colorBkgr());
                    page.setBackgroundColor(bg);
                }

                if (item.description.isEmpty()) {
                    txt.setVisibility(View.GONE);
                } else {
                    txt.setVisibility(View.VISIBLE);
                }

                //if (item.level > 0) {
                //    txt.setText(Html.fromHtml(item.html));
                //} else {
                    txt.setText(item.description);
                //}

                txt.setLinkTextColor(Color.GRAY);
                Linkify.addLinks(txt, Linkify.ALL);

                ans.setText(" + "+String.valueOf(item.answers));
                dat.setText(item.date);
              
                //Log.i("ThreadAdapter","author "+item.authorIndex);
                Author a = DataCollector.authors().author(item.authorIndex);
                if (a != null) {
                    //Log.i("ThreadAdapter","author "+a.name);
                    aut.setText(a.name);
                    
                    ImageView icView = (ImageView) v.findViewById(DataCollector.resourceId(ResourceProvider.USER_ICON));
                    icView.setImageResource(DataCollector.resourceId(ResourceProvider.USER_DRAW_ICON));

                    if (a.icon != null && !a.icon.isEmpty()) {
                        
                        String ic = a.icon;
                        if (ic.startsWith("//")) {
                            ic = "http:"+ic;
                        }
                       
                        Bitmap bm = DataCollector.loadImageWithNotif(ic);
                        if (bm != null) {  // got it from cache
                            icView.setImageBitmap(bm);
                        }
                    }
                }
            } else {
                txt.setText("");
                ans.setText("");
                dat.setText("");
                page.setVisibility(View.GONE);
                page.loadData("","text/html",null);
            }
        }
        return v;
    } 
    
    @Override
    public void onClick(View arg0) {
        Log.i("ThreadAdapter", "onItemClick " + ((TextView) arg0).getText().toString());

        int idx = DataCollector.authors().authorIdxByName(((TextView) arg0).getText().toString());
        if (idx >= 0) {
            DataCollector.sendGlobal(BaseProvider.LOAD_USER_INFO, idx);
        }
    }
;

    public void update() {

        items.clear();

        synchronized (DataCollector.provider()) {

            Vector<ForumThreadData> ff = DataCollector.provider().threadData();
            for (int i = 0; i < ff.size(); i++) {
                items.add(ff.get(i));
            }
        }

        this.notifyDataSetChanged();
    }
}
