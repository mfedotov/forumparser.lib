//
// Copyright (C) 2016-2017 Mikhail Fedotov <anyremote@mail.ru>
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA. 
//

package ar.forum;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.os.Bundle;
import android.text.Html;
import android.text.util.Linkify;
import android.util.Log;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TableLayout;
import android.widget.TextView;
import ar.forum.utils.ResourceProvider;

public class About extends BaseFragment {

	public About() {	} 
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
	  
	    Log.i("About","onCreateView");
        if (DataCollector.isStopped()) {
            return null;
        }

        View rootView = inflater.inflate(DataCollector.resourceId(ResourceProvider.LAYOUT_ABOUT), container, false);

        TableLayout tl = (TableLayout) rootView.findViewById(DataCollector.resourceId(ResourceProvider.VIEW_ABOUT));
        tl.setBackgroundResource(DataCollector.colorBkgr());

        TextView tv = (TextView) rootView.findViewById(DataCollector.resourceId(ResourceProvider.INFO_TEXT));
        tv.setTextColor(getResources().getColor(DataCollector.colorText()));
        tv.setText(Html.fromHtml("<h3>"+getResources().getString(DataCollector.resourceId(ResourceProvider.APP_NAME))+"</h3>"+
                readRawTextFile(DataCollector.resourceId(ResourceProvider.RAW_INFO))));
        tv.setLinkTextColor(Color.GRAY);
        
        Linkify.addLinks(tv, Linkify.ALL);
       
        int bg = getResources().getColor(DataCollector.colorBkgr());
        int fg = getResources().getColor(DataCollector.colorText());
        //String  colorize = "<body style=\"background-color:#"+ Integer.toHexString(bg).substring(2)
		//		         + ";color:#" + Integer.toHexString(fg).substring(2) + "\">\n";

        WebView legal = (WebView) rootView.findViewById(DataCollector.resourceId(ResourceProvider.LEGAL_TEXT));
        legal.loadData(/*colorize*/"<body> "+readRawTextFile(DataCollector.resourceId(ResourceProvider.RAW_LEGAL))+
                           "\n</body>", "text/html; charset="+DataCollector.provider().encoding(), null);
		//Log.i("About","onCreateView "+readRawTextFile(DataCollector.resourceId(ResourceProvider.RAW_LEGAL)));
		//legal.setText(Html.fromHtml(colorize+readRawTextFile(DataCollector.resourceId(ResourceProvider.RAW_LEGAL))+"\n</body>"));
		//legal.setLinkTextColor(Color.GRAY);
        legal.setBackgroundColor(bg);

        return rootView;
	}

	public String readRawTextFile(int id) {
		
	    Log.i("About","readRawTextFile");
	    
	    InputStream inputStream = getResources().openRawResource(id);
		
		InputStreamReader in = new InputStreamReader(inputStream);
		
		BufferedReader buf = new BufferedReader(in);
		
		String line;
		StringBuilder text = new StringBuilder();
		
		try {
			while ((line = buf.readLine()) != null)
				text.append(line);
		} catch (IOException e) {
			return null;
		}
		return text.toString();
	}
}
