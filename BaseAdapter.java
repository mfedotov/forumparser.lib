//
//Copyright (C) 2016-2017 Mikhail Fedotov <anyremote@mail.ru>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA. 
//

package ar.forum;

import java.util.ArrayList;

import android.widget.ArrayAdapter;
import android.content.Context;

public class BaseAdapter<T> extends ArrayAdapter<T> {
    
    public Context context;
    public ArrayList<T> items;
    
    public BaseAdapter(Context context, int textViewResourceId, ArrayList<T> items) {
        super(context, textViewResourceId, items);
        this.context = context;
        this.items = items;
    }

    // used in serialization
    public ArrayList<T> items() {
        return items;
    }

    public T get(int i) {
        if (i>=0 &&i<items.size()) {
            return items.get(i);
        }
        return null;
    }
    
    public void remove(int i) {
        if (i>=0 &&i<items.size()) {
            items.remove(i);
            this.notifyDataSetChanged();
        }
    }
    
    public int size() {
        return items.size();
    }
}
