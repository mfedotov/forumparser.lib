//
// Copyright (C) 2016 Mikhail Fedotov <anyremote@mail.ru>
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA. 
//

package ar.forum;

import android.util.Log;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import ar.forum.utils.ForumThreadData;
import ar.forum.utils.ResourceProvider;

public class NewMsgFragment extends BaseFragment {
    
    ForumThreadData data = null;
    int             uid  = -1;
    
    CheckBox quoteCb = null;
    EditText text    = null;
    EditText subj    = null;
    
    public NewMsgFragment() {
    }

    public void setup(ForumThreadData v, int u) {
        data = v;
        uid  = u;
    }
   
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        
        Log.i("NewMsgFragment","onCreateView");

        View rootView = inflater.inflate(DataCollector.resourceId(ResourceProvider.VIEW_NEWMSG), container, false);  
        
        LinearLayout l = (LinearLayout) rootView.findViewById(DataCollector.resourceId(ResourceProvider.LAYOUT_NEWMSG));
        //l.setBackgroundResource(DataCollector.bgDark());
        
        TextView tv = (TextView)rootView.findViewById(DataCollector.resourceId(ResourceProvider.SUBJ_LABEL));
        tv.setTextColor(getActivity().getResources().getColor(DataCollector.colorText()));
        
        tv = (TextView)rootView.findViewById(DataCollector.resourceId(ResourceProvider.MSG_LABEL));
        tv.setTextColor(getActivity().getResources().getColor(DataCollector.colorText()));
        
        subj = (EditText) rootView.findViewById(DataCollector.resourceId(ResourceProvider.SUBJ_EDIT));
        subj.setBackgroundResource(DataCollector.colorBkgr());
        subj.setText("Re: "+(data != null ? data.description : ""));
        
        text = (EditText) rootView.findViewById(DataCollector.resourceId(ResourceProvider.MSG_EDIT));
        text.setBackgroundResource(DataCollector.colorBkgr());
        
        quoteCb = (CheckBox) rootView.findViewById(DataCollector.resourceId(ResourceProvider.QUOTE));
        
        if (data == null || data.html == null) {
            quoteCb.setEnabled(false);
        } else {
            quoteCb.setOnClickListener(new OnClickListener()
            {
                  @Override
                  public void onClick(View v)
                  {
                      Log.i("NewMsgFragment","onClick quote");
                      if (data != null && data.html != null) {
                          if (quoteCb.isChecked()) {
                              text.setText("> "+data.html);
                          } else {
                              text.setText("");
                          }
                      }
                  } 
            });
        }

        Button button = (Button) rootView.findViewById(DataCollector.resourceId(ResourceProvider.MSG_SEND));
        button.setOnClickListener(new OnClickListener()
        {
              @Override
              public void onClick(View v)
              {
            	  ForumThreadData d = new ForumThreadData();
            	  d.description = subj.getText().toString();
            	  d.html = text.getText().toString();
            	  
            	  int msgtype = -1;
            	  if (uid >= 0) {
                	   msgtype = BaseProvider.SEND_MESSAGE;
                	   d.authorIndex = uid;
                   } else if (data != null) {
                	   msgtype = BaseProvider.SEND_POST;
                       d.href = data.href;
                   }
            	   if (msgtype >= 0) {
            	       DataCollector.sendGlobal(msgtype, d);
                   }
                   getFragmentManager().popBackStack();
              } 
        }); 

        return rootView;
    }

    @Override
    public void onStart() {
        Log.i("NewMsgFragment", "onStart");
        super.onStart();
        setTitle(getResources().getString(DataCollector.resourceId(ResourceProvider.NEW_MSG)));
    }

}
