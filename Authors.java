//
//Copyright (C) 2016 Mikhail Fedotov <anyremote@mail.ru>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA. 
//

package ar.forum;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import httpimage.HttpImageManager;
import android.net.Uri;
import android.util.Log;
import ar.forum.utils.Author;

public class  Authors  {
    
    public static final int CURUSER_ID = -1;

    private Vector<Author> authors = new Vector<Author>();
    
    private Author curAuthor = new Author();
    
    private Map<String, Integer> authorsNameMap = new HashMap<String, Integer>();
    private Map<String, Integer> authorsHrefMap = new HashMap<String, Integer>();
   
    public Author author(int i) {
        if (i>=0 && i < authors.size()) {
            return authors.elementAt(i);
        }
        return null;
    }

    public void setAuthor(int i, Author a) {
        
        if (i == CURUSER_ID) {
            curAuthor = a;
        }
        if (i>=0 && i < authors.size()) {
            authors.set(i,a);
        }
    }

    public Author author() {
        return curAuthor;
    }
    
    public Author authorById(String id) {
        for (int i=0;i<authors.size();i++) {
            if (authors.elementAt(i).id.equals(id)) {
                return authors.elementAt(i);
            }
        } 
        return null;
    }
    
    public int authorIdxByName(String name) {
        Integer v = authorsNameMap.get(name);
        return v == null ? -1 : v.intValue();
    }
    
    public int authorIdxByHref(String href) {
        Integer v = authorsHrefMap.get(href);
        return v == null ? -1 : v.intValue();
    }
    
    public int addAuthorIfNew(boolean useHttps, String name, String icon, String href) {
              
        //Log.i("addAuthorIfNew", name + " " + href+" "+icon);

        Integer v = authorsNameMap.get(name);
        if (v != null) {
            //Log.i("addAuthorIfNew","EXISTS");
            Author au = author(v);
            //Log.i("addAuthorIfNew","EXISTED "+au.icon+" "+au.href);
            if (icon != null && !icon.isEmpty() && (au.icon == null || au.icon.isEmpty())) {
                au.icon = icon;
            }
            if (href != null && !href.isEmpty() && (au.href== null || au.href.isEmpty())) {
                au.href = href;
            }
            return v.intValue();
        }
        //Log.i("addAuthorIfNew ADD ", name + " " + href+" "+icon);
        String proto = (useHttps ? "https:" : "http:");

        String id = "";
        if (href != null) { 
            int lastcomma = href.lastIndexOf(",");
            int lastidx = href.length();
            if (href.endsWith("html")) {
                lastidx -= 5;
            }
            id = href.substring(lastcomma+1);
            if (href.startsWith("//")) {
                href = proto+href;
            }
        }
        if (icon != null && icon.startsWith("//")) {
            icon = proto+icon;
        }
       
        //Log.i("addAuthorIfNew", name + " " + id + " " +href+" "+icon);
        
        Author oneMore = new Author();
        oneMore.name = name;
        oneMore.id   = id;
        oneMore.href = href;
        oneMore.icon = icon;
        oneMore.html = "";
        
        authors.add(oneMore);
        
        int idx = authors.size() - 1;
        
        authorsNameMap.put(name, idx);
        authorsHrefMap.put(href, idx);

        if (icon != null && !icon.isEmpty()) {
            
            String ic = icon;
            if (ic.startsWith("//")) {
                ic = "http:"+ic;
            }
        
            DataCollector.imageDownloader().loadImage(
                new HttpImageManager.LoadRequest(Uri.parse(ic))); /*, new HttpImageManager.OnLoadResponseListener() {

                @Override
                public void onLoadResponse(LoadRequest r, Bitmap data) {
                    Log.i("ForumAdapter","getView ICON LOAD OK");
                }
    
                @Override
                public void onLoadProgress(LoadRequest r, long totalContentSize, long loadedContentSize) { }
    
                @Override
                public void onLoadError(LoadRequest r, Throwable e) {
                    Log.i("ForumAdapter","getView ICON LOAD ERROR "+e.getMessage());
                }
            }));*/
        }

        return idx;
    }
}
