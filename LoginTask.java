//
// Copyright (C) 2016 Mikhail Fedotov <anyremote@mail.ru>
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA. 
//

package ar.forum;

import java.io.DataOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.LinkedHashMap;
import java.util.Map;

import android.util.Log;

public class LoginTask implements Runnable {
            
    private String  m_url;     
      
    public LoginTask(String url) {
        m_url  = url;  
    }
        
    public void run() {
        Log.i("LoginTask","get login page");
        Downloader.sendGet(m_url, true);
        Log.i("LoginTask","post LOGIN page");
        sendPost();
    }
    
    public void sendPost() {   
         
        URL url;
        try {
            url = new URL(m_url);
        } catch (MalformedURLException e1) {
            Log.i("LoginTask", "sendPost exception "+e1.getMessage());
            DataCollector.sendGlobal(BaseProvider.LOGIN_ERR, e1.getMessage());
            return;
        }
        Map<String,Object> params = new LinkedHashMap<String,Object>(); 
        
        //Log.i("sendPost", "credentials "+DataCollector.prefs().getUser()+" "+DataCollector.prefs().getPassword());
        params.put(DataCollector.provider().formDataUser(), DataCollector.prefs().getUser());
        params.put(DataCollector.provider().formDataPass(), DataCollector.prefs().getPassword());
        
        StringBuilder postData = new StringBuilder();
        byte[] postDataBytes;
        try {
            for (Map.Entry<String,Object> param : params.entrySet()) {

                if (postData.length() != 0) {
                    postData.append('&');
                }
                postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                postData.append('=');
                postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
            }
            postDataBytes = postData.toString().getBytes("UTF-8");
       
        } catch (UnsupportedEncodingException e1) {
            Log.i("LoginTask", "sendPost exception (1) "+e1.getMessage());
            DataCollector.sendGlobal(BaseProvider.LOGIN_ERR, e1.getMessage());
            return;
        }
        
        try {

            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            conn.setInstanceFollowRedirects(false);
            HttpURLConnection.setFollowRedirects(false);
            
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");         
            conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
            
            conn.setDoOutput(true);
            conn.setDoInput(true);
           
            // Add cookies, if any
            if (DataCollector.cookies().size() > 0) {
                //Log.i("LoginTask", "sendPost add cookies #" + DataCollector.cookies().size());
                //Log.i("LoginTask", "sendPost cookies " +getCookies());
                conn.setRequestProperty("Cookie", DataCollector.cookies().getCookies());
            }
         
            DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
            wr.write(postDataBytes);
            wr.flush();
            wr.close();
            
            //Log.i("LoginTask", "sendPost DATA SEND");
            
            int code = conn.getResponseCode();
             
            //Log.i("LoginTask","sendPost Response Code: " + code);
            if (code != HttpURLConnection.HTTP_MOVED_TEMP &&
                code != HttpURLConnection.HTTP_MOVED_PERM &&
                code != HttpURLConnection.HTTP_SEE_OTHER) {

                Log.i("LoginTask","sendPost: failed to login");
                DataCollector.sendGlobal(BaseProvider.LOGIN_ERR, DataCollector.WRONG_PASS);
                return; 
            }
            
            DataCollector.cookies().storeCookies(conn);
            
            while (code == HttpURLConnection.HTTP_MOVED_TEMP  || 
                   code == HttpURLConnection.HTTP_MOVED_PERM  || 
                   code == HttpURLConnection.HTTP_SEE_OTHER) {
                    
                    String newUrl = conn.getHeaderField("Location");
                    if (!newUrl.isEmpty() && newUrl.charAt(0) == '/') {
                        newUrl = conn.getURL().getProtocol() + "://" + conn.getURL().getHost() + newUrl;              
                    }
                    Log.i("LoginTask","sendPost ... redirected "+newUrl); 
                    
                    conn = Downloader.createConnectionGET(newUrl);  // can be null
                    
                    code = conn.getResponseCode();  
                    DataCollector.cookies().storeCookies(conn);
                    
                    Log.i("LoginTask","sendPost redirected response Code: " + code + " (cookies #" + DataCollector.cookies().size() + ")"); 
            }

            conn.disconnect();

        } catch (Exception e) {
            Log.i("LoginTask","sendPost exception (2) "+e.getMessage());
            DataCollector.sendGlobal(BaseProvider.LOGIN_ERR, e.getMessage());
            return;
        }
        
        Log.i("LoginTask","sendPost LOGIN_OK");
        DataCollector.sendGlobal(BaseProvider.LOGIN_OK);
        return;
    }
}
