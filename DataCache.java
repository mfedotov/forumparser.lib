//
//Copyright (C) 2016 Mikhail Fedotov <anyremote@mail.ru>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA. 
//

package ar.forum;

import java.util.Vector;
import java.util.Map;
import java.util.HashMap;

import android.util.Log;
import ar.forum.utils.ContentItem;
import ar.forum.utils.FavoriteItem;
import ar.forum.utils.Feed;
import ar.forum.utils.ShortcutItem;

public class DataCache {
    
    static Vector<ContentItem>  forumList = new Vector<ContentItem>();
    static Vector<FavoriteItem> favorites = new Vector<FavoriteItem>();
    static Vector<ShortcutItem> shortcuts = new Vector<ShortcutItem>();

    static Map<Integer, Vector<ContentItem>> cachedData = new HashMap<Integer,Vector<ContentItem>>();

    static Map<Integer,Feed>        feeds     = new HashMap<Integer,Feed>();

    static int curForumPage;
    static int curThreadPage;
    static String curThread;
    static int curUserPage;
    
    static String curURL;

    //
    // Forums list
    //
    public ContentItem forum(int i) {
        if (i>=0 && i<forumList.size()) {
            return forumList.get(i);
        }
        return null;
    }

    public int forumIndex(String href) {
        for (int i=0; i<forumList.size();i++) {
            ContentItem ci= forumList.get(i);
            if (ci.href == href) {
                return i;
            }
        }
        return -1;
    }
    public boolean forumsEmpty() {
        return forumList.isEmpty();
    }
    
    public int forumsSize() {
        return forumList.size();
    }
    
    public void clearForums() {
        forumList.clear();
    }
        
    public void addForum(String title, String url, int level, boolean exp) {
        //Log.i("DataCache", "addForum >"+h+"< >"+t+"< >"+u+"<");
        ContentItem it = new ContentItem();
        it.idx   = forumList.size();
        it.title = title;
        it.href  = url; 
        it.level = level; 
        it.expanded = exp;
        
        forumList.add(it);
    }

    public String forumTitle(int id) {
        ContentItem ci = forum(id);   
        return (ci == null ? "" : ci.title);
    }

    public void resetForum() {
        curForumPage = 1;
    }
   
    public int incrPage() {
        curForumPage++;  
        return curForumPage;    
    }

    //
    // Generic Cache API
    //
    public void addCache(int id) {
        if (cachedData.containsKey(id)) {
            cachedData.get(id).clear();
        } else {
            Vector<ContentItem> c = new Vector<ContentItem>();
            cachedData.put(id,c);
        }
    }

    public void addCacheData(int id, ContentItem ci) {
        if (cachedData.containsKey(id)) {
            cachedData.get(id).add(ci);
        }
    }

    public int cacheSize(int id) {
        return cachedData.get(id).size();
    }

    public void resetCache(int id) {
        cachedData.get(id).clear();
    }

    public Vector<ContentItem> cachedData(int id) {
        return cachedData.get(id);
    }

    public ContentItem cachedItem(int id, int i) {
        Vector<ContentItem> v = cachedData(id);
        if (i>=0 && i<v.size()) {
            return v.get(i);
        }
        return null;
    }

    //
    // Feeds API (Main feed, Category feed, Search feed, Blog feed, Shop feed ...
    //
    private void addFeedImplicit(int id) {
        Feed f = new Feed(id);
        feeds.put(id,f);
    }

    public void addFeed(int id) {
        if (feeds.containsKey(id)) {
            feeds.get(id).resetPage();
            feeds.get(id).setCategory("");
        } else {
            addFeedImplicit(id);
        }
    }

    public void setFeedCategory(int id, String v) {
        if (!feeds.containsKey(id)) {
           addFeedImplicit(id);
        }
        feeds.get(id).setCategory(v);
    }

    public void resetFeedPage(int id) {
        if (!feeds.containsKey(id)) {
            addFeedImplicit(id);
        }
        feeds.get(id).resetPage();
    }

    public int incrFeedPage(int id) {
        if (!feeds.containsKey(id)) {
            addFeedImplicit(id);
        }
        return feeds.get(id).incrPage();
    }

    public String getFeedData(int id) {
        if (!feeds.containsKey(id)) {
            addFeedImplicit(id);
        }
        return feeds.get(id).category();
    }

    //
    // Threads
    //
    public void resetThread() {
        curThreadPage = 1;
    }

    public int incrThreadPage() {
        curThreadPage++;
        return curThreadPage;
    }

    public String currentThread() {
        return curThread;
    }
    
    public void setCurrentThread(String v) {
        curThread = v;
    }
 
    public String currentURL() {
        return curURL;
    }
    
    public void setCurrentURL(String v) {
        curURL = v;
    }   
   
    public void resetUserPage() {
        curUserPage = 1;      
    }
   
    public int incrUserPage() {
        curUserPage++;  
        return curUserPage;    
    }
    
    public FavoriteItem favorite(int i) {
        if (i>=0 && i<favorites.size()) {
            return favorites.get(i);
        }
        return null;
   }
   
    public int favoritesSize() {
        return favorites.size();
    }
    
    public void clearFavorites() {
        favorites.clear();
    }
 
    // load from SharedPrefs, no needs to save back
    public void addFavorite(String t, String h) {
        
        FavoriteItem c = new FavoriteItem(); 
        c.title = t;
        c.href  = h;
        favorites.add(c);
    }

    public void addFavorite(int idx) {
        Log.i("addFavorites","#"+idx); 
        
        if (idx>=0 && idx<forumList.size()) {
            
            ContentItem c = forumList.get(idx);
            
            Log.i("addFavorites",c.href+":"+c.title);
            
            for (int i=0;i<favorites.size();i++) {

                FavoriteItem f = favorites.get(i);

                if (c.href.compareTo(f.href) == 0) {
                    return;  // already exists
                }
            }
            
            FavoriteItem f = new FavoriteItem();
            f.title = c.title;
            f.href  = c.href; 
             
            favorites.add(f);
        }
        DataCollector.sendGlobal(BaseProvider.SAVE_FAVS);
    }
    
    public void removeFavorite(int idx) {
        //Log.i("addFavorites","#"+idx); 
        if (idx>=0 && idx<favorites.size()) {
            favorites.remove(idx);
        }
        DataCollector.sendGlobal(BaseProvider.SAVE_FAVS);
    }
    
    public ShortcutItem shortcut(int i) {
        if (i>=0 && i<shortcuts.size()) {
            return shortcuts.get(i);
        }
        return null;
    }
    
    public int shortcutsSize() {
        return shortcuts.size();
    }
    
    public void clearShortcuts() {
        shortcuts.clear();
    }

    public void addShortcut(String t, String h, int type, boolean save) {
        addShortcut(t,h,"",type,save);
    }
    public void addShortcut(String t, String h, String ic, int type, boolean save) {

        Log.i("DataCache::addShortcut",t+" "+h+" "+type);
        ShortcutItem c = new ShortcutItem(); 
        c.title = t;
        c.href  = h;
        c.iconUrl = ic;
        c.type  = type;

        shortcuts.add(c);
        if (save) {
            DataCollector.sendGlobal(BaseProvider.SAVE_SHORTCUTS);
        }
    }   
    
    public void removeShortcut(int idx) {
        if (idx >= 0 && idx <shortcuts.size()) {
            shortcuts.remove(idx);
            DataCollector.sendGlobal(BaseProvider.SAVE_SHORTCUTS);
        }
    }
}
