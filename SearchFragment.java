//
//Copyright (C) 2017 Mikhail Fedotov <anyremote@mail.ru>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//

package ar.forum;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.view.LayoutInflater;
import android.widget.TextView;

import ar.forum.utils.ResourceProvider;

public class SearchFragment extends DialogFragment {

    TextView m_search;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setStyle(DialogFragment.STYLE_NORMAL, 0);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(DataCollector.resourceId(ResourceProvider.SEARCH));

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(DataCollector.resourceId(ResourceProvider.SEARCH_LAYOUT),null);
        m_search = (TextView) v.findViewById(DataCollector.resourceId(ResourceProvider.SEARCH_TEXT));

        builder.setView(v);

        builder.setNegativeButton(DataCollector.resourceId(ResourceProvider.CANCEL), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //Log.i("SearchFragment","CANCEL");
            }
        });

        builder.setPositiveButton(DataCollector.resourceId(ResourceProvider.SEARCH), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //Log.i("SearchFragment","OK");
                if (m_search != null) {
                    DataCollector.sendGlobal(BaseProvider.LOAD_SEARCH, m_search.getText().toString().replace(' ','+'));
                }
            }
        });

        return builder.create();
    }

    @Override
    public void onCancel (DialogInterface dialog) {
        dismiss();
    }
}