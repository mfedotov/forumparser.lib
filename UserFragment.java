//
//Copyright (C) 2016 Mikhail Fedotov <anyremote@mail.ru>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA. 
//

package ar.forum;

import httpimage.HttpImageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import ar.forum.utils.Author;
import ar.forum.utils.ResourceProvider;

public class UserFragment extends BaseFragment {
    
    private int       m_uid = -1;
    private ImageView m_ava = null;

    public UserFragment() {
    }

    public void setup(int uid) {
        m_uid = uid;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        
        Log.i("UserFragment","onCreateView"); 
        DataCollector.setStage(BaseProvider.SHOW_USER_INFO, m_uid);
        
        View rootView = inflater.inflate(DataCollector.resourceId(ResourceProvider.LAYOUT_USER), container, false);
        rootView.setBackgroundResource(DataCollector.colorBkgr());

        boolean msgList = DataCollector.provider().supportUserMessagesList();
        
        WebView   wv = (WebView)   rootView.findViewById(DataCollector.resourceId(ResourceProvider.VIEW_WEB));
        m_ava = (ImageView) rootView.findViewById(DataCollector.resourceId(ResourceProvider.AVATAR));
        TextView  tv = (TextView)  rootView.findViewById(DataCollector.resourceId(ResourceProvider.NICK));
        Button    b  = (Button)    rootView.findViewById(DataCollector.resourceId(ResourceProvider.B_ALL_MSGS));

        if (msgList) {
            b.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    DataCollector.sendGlobal(BaseProvider.LOAD_USER_MSGS, m_uid);
                }
            });
            b.setBackgroundResource(DataCollector.colorBkgr());
            b.setTextColor(getResources().getColor(DataCollector.colorText()));
        } else {
            b.setVisibility(View.GONE);
        }

        tv.setTextColor(getResources().getColor(DataCollector.colorText()));
        
        synchronized (DataCollector.provider()) {

            String html = null;
            String ava = null;
            String nick = "";

            if (m_uid >= 0) {
                Author user = DataCollector.authors().author(m_uid);
                if (user != null) {
                    html = user.html;
                    ava = user.icon;
                    nick = user.name;
                }
            } else if (m_uid == Authors.CURUSER_ID) {   // current user
                Author user = DataCollector.authors().author();
                html = user.html;
                ava = user.icon;
                nick = user.name;

                Log.i("UserFragment", "onCreateView USER " + nick + " >" + user.id + "< "); // + html);

                if (user.id.isEmpty()) {
                    b.setVisibility(View.GONE);
                } else {
                    b.setText(DataCollector.resourceId(ResourceProvider.USER_MSGS));
                }
            }

            if (html != null) {
                int bg = getResources().getColor(DataCollector.colorBkgr());
                int fg = getResources().getColor(DataCollector.colorText());

                //String colorize = "<body style=\"background-color:#" + Integer.toHexString(bg).substring(2) + ";color:#" + Integer.toHexString(fg).substring(2) + "\">\n";
                //Log.i("UserFragment", "COLORS " + colorize);
                wv.loadData("<style>img{display: inline; height: auto; max-width: 100%;}</style><body> " + /*colorize +*/ html + "\n</body>",
                        "text/html; charset=" + DataCollector.provider().encoding(), null);
                wv.setBackgroundColor(bg);

                WebSettings webSettings = wv.getSettings();

                webSettings.setJavaScriptEnabled(false);
                webSettings.setJavaScriptCanOpenWindowsAutomatically(false);
                webSettings.setSupportMultipleWindows(false);
                webSettings.setSupportZoom(true);
                webSettings.setBuiltInZoomControls(true);
                webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);

                wv.setVerticalScrollBarEnabled(false);
                wv.setHorizontalScrollBarEnabled(false);
            }

            m_ava.setImageResource(DataCollector.resourceId(ResourceProvider.USER_DRAW_ICON));
            if (ava != null && !ava.isEmpty()) {

                Bitmap bm = DataCollector.loadImageWithNotif(ava);
                if (bm != null) { // got it from cache
                    m_ava.setImageBitmap(bm);
                }
            }

            tv.setText(nick);
        }

        return rootView;
    }

    @Override
    public void onStart() {
        Log.i("UserFragment", "onStart");
        super.onStart();
        setTitle(getResources().getString(DataCollector.resourceId(ResourceProvider.PROFILE)));
    }

    public void update() {
        Log.i("UserFragment","update " + m_uid);  // user icon was downloaded asynchronously

        Author user = m_uid >= 0 ?
                DataCollector.authors().author(m_uid) :
                DataCollector.authors().author();  // current user

        if (!user.icon.isEmpty()) {
            Log.i("UserFragment", "update icon " + user.icon);
            Bitmap bm = DataCollector.imageDownloader().loadImage(new HttpImageManager.LoadRequest(Uri.parse(user.icon)));
            if (bm != null) { // got it from cache
                m_ava.setImageBitmap(bm);
            }
        }

    }
}
