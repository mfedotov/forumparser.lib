//
//Copyright (C) 2016-2017 Mikhail Fedotov <anyremote@mail.ru>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA. 
//

package ar.forum;

import java.util.ArrayList;
import java.util.Vector;

import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import ar.forum.utils.ForumThreadData;
import ar.forum.utils.Prefs;
import ar.forum.utils.ResourceProvider;

public class MsgsFragment extends BaseFragment 
                           implements OnItemClickListener {
    
    private ListView m_list = null;
    private ForumAdapter m_dataSource = null;
    private Parcelable m_listSavedState = null;
    int     type;

    public MsgsFragment() {
        super();
        this.type = ForumAdapter.ADAPT_MSGLIST;
    }
    
    public void setType(int type) {
        this.type = type;
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        
        Log.i("MsgsFragment","onCreateView");
 
        DataCollector.setStage(type == ForumAdapter.ADAPT_MSGLIST ? BaseProvider.SHOW_MSGS : BaseProvider.SHOW_MSG);
        
        View rootView = inflater.inflate(DataCollector.resourceId(ResourceProvider.LAYOUT_THREADS), container, false); 
        
        m_list = (ListView) rootView.findViewById(DataCollector.resourceId(ResourceProvider.THREAD_LIST));
        m_list.setBackgroundResource(DataCollector.colorBkgr());
        m_list.setDivider(getResources().getDrawable(DataCollector.colorBkgr()));
        m_list.setDividerHeight(3);

        onCreateBaseView(m_list); // set scroll counters
        
        createAdapter();

        m_list.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        //m_list.setOnItemSelectedListener(this);
        m_list.setOnItemClickListener(this);

        return rootView;
    }
    
    @Override
    public void onSaveInstanceState(final Bundle outState) {
        Log.i("HeadersFragment", "onSaveInstanceState");
        super.onSaveInstanceState(outState);

        if (m_list != null) {
            m_listSavedState = m_list.onSaveInstanceState();
            outState.putParcelable("msgs", m_listSavedState);
        }
    }

    @Override
    public void onActivityCreated (Bundle savedInstanceState) {
        Log.i("MsgsFragment","onActivityCreated"); 
        
        if (savedInstanceState != null) {
            Log.i("HeadersFragment", "onActivityCreated have saved state");
            //probably orientation change
            createAdapter();
            m_listSavedState = savedInstanceState.getParcelable("msgs");

        } else if (m_dataSource != null) {
            //returning from backstack, data is fine, do nothing
        } else {
            //newly created, compute data
            createAdapter();
        }

        if (m_list != null) {

            if (m_listSavedState != null) {
                 m_list.onRestoreInstanceState(m_listSavedState);
            }
            if (type == ForumAdapter.ADAPT_MSGTHREAD) {
                registerForContextMenu(m_list);
            }
        }
        setHasOptionsMenu(true);        
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        Log.i("MsgsFragment", "onStart");
        super.onStart();

        String v = "";
        if (type == ForumAdapter.ADAPT_MSGLIST) {
            v = getResources().getString(DataCollector.resourceId(ResourceProvider.USER_MSGS));
        } else {
            v = getResources().getString(DataCollector.resourceId(ResourceProvider.MESSAGE));
        }
        setTitle(v);
    }
   
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		if (type == ForumAdapter.ADAPT_MSGLIST) {
			final ForumThreadData item = m_dataSource.get(arg2);
			if (item != null && !item.href.isEmpty()) {
			    DataCollector.sendGlobal(BaseProvider.LOAD_MSG, arg2);
			}
		}
	}
    
    @Override
    public void onCreateContextMenu (ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        Log.i("MsgsFragment","onCreateContextMenu"); 
        
        int mid = v.getId();
        if (mid == DataCollector.resourceId(ResourceProvider.THREAD_LIST) && 
            type == ForumAdapter.ADAPT_MSGTHREAD) {

            DataCollector.setMenuByFragmentId(getID());

            menu.clear();
          
            final AdapterContextMenuInfo info = (AdapterContextMenuInfo) menuInfo;
            int idx = info.position; 
            
            ForumThreadData ci = m_dataSource.get(idx);
            if (ci == null) {
                return;
            }
            
            //menu.setHeaderIcon(getResources().getDrawable(R.drawable.ic_fav));
            final TextView tv = (TextView) info.targetView.findViewById(DataCollector.resourceId(ResourceProvider.THREAD_TITLE));
            if (tv != null) {
                menu.setHeaderTitle(tv.getText());
            }
           
            MenuInflater inflater = getActivity().getMenuInflater();
            inflater.inflate(DataCollector.resourceId(ResourceProvider.MSG_CTX_MENU), menu);
        }
    }

    // Handle context menu, opened by long-click
    @Override
    public boolean onContextItemSelected(MenuItem item) {

        if (DataCollector.menuByFragmentId() != getID()) {
            return false;
        }

        final AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
        
        int mid = item.getItemId();
        
        Log.i("MsgsFragment","onContextItemSelected "+info.position+" "+mid); 
       
        if (mid == DataCollector.resourceId(ResourceProvider.ANSWER_SEND)) { 
        	  int uid = -1;
        	  
        	  // only user and other author exists in the thread
        	  for (int i=0;i<m_dataSource.size();i++) {
                   final ForumThreadData ft = m_dataSource.get(i);
                   if (ft.authorIndex >= 0) {
                        uid = ft.authorIndex;
                        break;
                   }
              }
        	  Log.i("MsgsFragment","onContextItemSelected "+info.position+" "+mid);
              if (uid >= 0) { 
                  DataCollector.sendGlobal(BaseProvider.PREPARE_MESSAGE, uid);
              }
              return true;
        }
        
        return super.onContextItemSelected(item);
    }
    
    public void createAdapter() {
        
        ArrayList<ForumThreadData> m_listItems = new ArrayList<ForumThreadData>();
        
        synchronized (DataCollector.provider()) {
            Vector<ForumThreadData> ff = (type == ForumAdapter.ADAPT_MSGLIST ?
                    DataCollector.provider().msgList() :
                    DataCollector.provider().msgThread());
            for (int i = 0; i < ff.size(); i++) {
                m_listItems.add(ff.get(i));
            }
        }

         Log.i("MsgsFragment","createAdapter #"+m_listItems.size());
         m_dataSource = new ForumAdapter(getActivity(),
                                          DataCollector.resourceId(ResourceProvider.THREAD_LIST_ITEM), 
                                          type, m_listItems);
         if (m_list != null) {
             m_list.setAdapter(m_dataSource);
         }
    }
    
    public void update() {
        Log.i("MsgsFragment","update"); 
        m_dataSource.update();
    }
    
    public BaseAdapter adapter() {
        return m_dataSource;
    }

    public void onZoom(int zoom) {   
        onZoom(zoom, Prefs.POST_TEXT);
    }

    public void onOverFling(int dir) {
        Log.i("MsgsFragment","onOverFling "+dir); 
        if (dir == BaseFragment.SWIPE_DOWN) {
            Log.i("MsgsFragment","NEED UPDATE"); 
            if (type == ForumAdapter.ADAPT_MSGLIST) {
                DataCollector.sendGlobal(BaseProvider.LOAD_MSGS_AGAIN);
            }
        }
    }
}
