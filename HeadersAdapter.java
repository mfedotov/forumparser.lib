//
//Copyright (C) 2016 Mikhail Fedotov <anyremote@mail.ru>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA. 
//

package ar.forum;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import ar.forum.utils.ContentItem;
import ar.forum.utils.Prefs;
import ar.forum.utils.ResourceProvider;
import httpimage.HttpImageManager;

public class HeadersAdapter extends BaseAdapter<ContentItem> {
    
    public HeadersAdapter(Context context, int textViewResourceId, ArrayList<ContentItem> items) {
        super(context, textViewResourceId, items);
    }
        
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
         //Log.i("HeadersAdapter","getView #"+position);
        
        final View v;
        
        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(DataCollector.resourceId(ResourceProvider.HEADER_LIST_ITEM), null);
        } else {
            v = convertView;
        }

        TextView  txt = (TextView) v.findViewById(DataCollector.resourceId(ResourceProvider.HEADER_TEXT));
        int ts = (int) txt.getTextSize();
        int textSize = DataCollector.prefs().textSize(Prefs.HEADER_TEXT);
        if (textSize < 0) { 
            //Log.i("HeadersAdapter","getView store text size "+ts);
            DataCollector.prefs().saveTextSize(null,Prefs.HEADER_TEXT,ts);
            DataCollector.sendGlobal(BaseProvider.SETUP_HEADER_SIZE, textSize);  // will save text size value again
        } else if (textSize != ts) {
            //Log.i("HeadersAdapter","getView set text size "+textSize);
            txt.setTextSize(textSize);
        }
        txt.setTextColor(this.context.getResources().getColor(DataCollector.colorText()));
        
        ImageView ic  = (ImageView) v.findViewById(DataCollector.resourceId(ResourceProvider.EXPAND_ICON));
         
        synchronized (items) {
            ContentItem item = items.get(position);
            if (item != null) {

                //Log.i("HeadersAdapter","getView ContentItem "+item.level + " " + item.title + " " + item.iconUrl);
                txt.setText(item.title);

                if (item.level == 0 && item.href.isEmpty()) {

                     txt.setGravity(Gravity.LEFT);
                     ic.setVisibility(View.VISIBLE);
                    
                     if (position < items.size()-1) {
                         ContentItem next = items.get(position+1);              
                         if (next.level > 0) {
                              ic.setImageResource(DataCollector.resourceId(ResourceProvider.ICON_UP));
                         } else {
                              ic.setImageResource(DataCollector.resourceId(ResourceProvider.ICON_DOWN));
                         }
                     } else {
                         ic.setImageResource(DataCollector.resourceId(ResourceProvider.ICON_DOWN));  
                     }

                } else {

                     if (item.iconUrl != null && !item.iconUrl.isEmpty() && item.iconUrl.startsWith("http")) {
                         txt.setGravity(Gravity.LEFT);

                         Bitmap bm = DataCollector.imageDownloader().loadImage(
                                 new HttpImageManager.LoadRequest(Uri.parse(item.iconUrl),
                                         new HttpImageManager.OnLoadResponseListener() {
                                             public void onLoadResponse(HttpImageManager.LoadRequest r, Bitmap data) {
                                                 DataCollector.sendGlobal(BaseProvider.UPDATE_VISIBLE, 0);
                                             }
                                             public void onLoadProgress(HttpImageManager.LoadRequest r,
                                                                        long totalContentSize,
                                                                        long loadedContentSize) {}
                                             public void onLoadError(HttpImageManager.LoadRequest r, Throwable e) {}
                                         }));
                         if (bm != null) { // got it from cache
                             ic.setImageBitmap(bm);
                         }
                     } else if (DataCollector.provider().iconInHeaders()) {
                         txt.setGravity(Gravity.LEFT);
                         ic.setImageResource(DataCollector.resourceId(ResourceProvider.APP_ICON));  
                     } else {
                         txt.setGravity(Gravity.RIGHT);
                         ic.setVisibility(View.GONE);
                     }
                }
            }
        }
        return v;
    }

    public void expandOrCollapse(String head, int arg2) {
        
        if (size() <= arg2) {
            return;
        }
 
        ContentItem it = get(arg2);
   
        //Log.i("HeadersFragment","onItemClick collapse/expand"); 
        int di = arg2+1;
        ContentItem next = (size() <= di ? null : get(di));
        
        if (next != null && next.level > 0) {    // need collapse
            //Log.i("HeadersFragment","onItemClick collapse"); 
            it.expanded = false;
            
            while (di < size()) {
                if (get(di).level > 0) {
                    remove(di);
                } else {
                    break;
                }
            }
        } else {                      // need expand
            //Log.i("HeadersFragment","onItemClick expand "+head); 
            
            it.expanded = true;
            
            int hi = it.idx;
            hi++;
            
            while (hi < DataCollector.cache().forumsSize()) {
               
                it = DataCollector.cache().forum(hi);
                
                //Log.i("HeadersFragment","onItemClick FORUM "+it.href);
                if (it.level == 0) {
                    break;
                }
                insert(it, di);
                di++;
                hi++;
            }
        }
        notifyDataSetChanged();
    }

}
