//
//Copyright (C) 2016 Mikhail Fedotov <anyremote@mail.ru>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA. 
//

package ar.forum;

//import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.ListView;
import ar.forum.utils.Prefs;

public class BaseFragment extends Fragment {

    public static final int ID_UNKNOWN    = -1;

    public static final int SWIPE_UP      = 1;
    public static final int SWIPE_DOWN    = 2;
    public static final int SWIPE_LEFT    = 3;
    public static final int SWIPE_RIGHT   = 4;
    public static final int SWIPE_ZOOMIN  = 5;
    public static final int SWIPE_ZOOMOUT = 6;

    public static final int TEXT_MIN_SIZE = 10;
    public static final int TEXT_MAX_SIZE = 40;
    
    private int overscrollFlingUpCount = 0;
    private int overscrollFlingDownCount = 0;
    
    private ItemScrollListener scrollListener = null;

    private int fragmentID;

    private static int idCounter = 1;

    public BaseFragment() {
        super();
        Log.i("BaseFragment", "BaseFragment FID=" + idCounter);
        fragmentID = idCounter;
        idCounter++;
    }

    public int getID() {
        return fragmentID;
    }
  
    public void onCreateBaseView(ListView list) {
        scrollListener = new ItemScrollListener();
        list.setOnScrollListener(scrollListener);
    }

    @Override
    public void onStart() {
        Log.i("BaseFragment", "onStart");
        super.onStart();
        //pushTitle("");
    }

    @Override
    public void onStop() {
        Log.i("BaseFragment", "onStop");
        super.onStop();
        //synchronized (DataCollector.provider()) {
        //    getActivity().setTitle(DataCollector.popTitle());
        //}
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        Log.i("BaseFragment", "onHiddenChanged "+hidden);
        super.onHiddenChanged(hidden);
    }

    @Override
    public void onPause() {
        Log.i("BaseFragment", "onPause");
        super.onPause();
    }

    public void onResumeFragment() {
        Log.i("BaseFragment", "onResumeFragment");
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        Log.i("BaseFragment", "onViewStateRestored");
        super.onViewStateRestored(savedInstanceState);
    }

    public void setTitle(String v) {
        getActivity().setTitle(v);
    }

    public boolean onGest(int dir) {
        Log.i("BaseFragment","onGest "+dir+" "+" "+overscrollFlingUpCount+" "+overscrollFlingDownCount);
        if (scrollListener != null && scrollListener.isAtStart() && dir == BaseFragment.SWIPE_DOWN) {
            //Log.i("BaseFragment","onGest onOverFling A0");
            if (overscrollFlingDownCount == 0) {
                overscrollFlingDownCount++;    
            } else {
                overscrollFlingDownCount = 0;
                //Log.i("BaseFragment","onGest onOverFling A");
                onOverFling(dir);
            }
        } else if (scrollListener != null && scrollListener.isAtEnd() && dir == BaseFragment.SWIPE_UP) {
            //Log.i("BaseFragment","onGest onOverFling B0");
            if (overscrollFlingUpCount == 0) {
                overscrollFlingUpCount++;    
            } else {
                overscrollFlingUpCount = 0;
                //Log.i("BaseFragment","onGest onOverFling B");
                onOverFling(dir);
            }
        } else if (dir == BaseFragment.SWIPE_ZOOMIN) {
            //Log.i("BaseFragment","onGest onZoom IN");
            onZoom(dir);
        } else if (dir == BaseFragment.SWIPE_ZOOMOUT) {
            //Log.i("BaseFragment","onGest onZoom OUT");
            onZoom(dir);
        }
        return false;
   }
    
    public void onZoom(int zoom) {   
    }
    
    public void onZoom(int zoom, int type) {
       int tsz = DataCollector.prefs().textSize(type);
       if (tsz < 0) {
           return;
       }
       if (zoom == BaseFragment.SWIPE_ZOOMOUT) {
           tsz -= 1;
       } else {
           tsz += 1;
       }
       int id = (type == Prefs.HEADER_TEXT ? BaseProvider.SETUP_HEADER_SIZE : BaseProvider.SETUP_POST_SIZE);
       DataCollector.prefs().saveTextSize(null, type, tsz); 
       DataCollector.sendGlobal(id, tsz);  // will save text size value again
       if (adapter() != null) {
           adapter().notifyDataSetChanged();
       }
   };

   
   public void update() {};
   
   public BaseAdapter adapter() {return null;}
    
   public void onOverFling(int dir) {};
}
