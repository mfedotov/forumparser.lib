//
// Copyright (C) 2016 Mikhail Fedotov <anyremote@mail.ru>
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA. 
//

package ar.forum;

import android.util.Log;

public class DownloadTask implements Runnable {
            
    private String  m_url;     
    private int     m_type = BaseProvider.UNDEFINED;
    private int     m_data = -1;
    
    public DownloadTask(String url, int type, int data) {
        m_url  = url;  
        m_type = type;          
        m_data = data;
    }
        
    public void run() {

        //Log.i("DownloadTask","run(): "+m_type+" "+m_data+" "+m_url);
        
        if (m_type == BaseProvider.UNDEFINED ||
            m_type == BaseProvider.LOGIN ||
            DataCollector.provider() == null) {
            
            DataCollector.sendGlobal(BaseProvider.DLOAD_RES, Downloader.LOAD_ERR);
            return;
        }
        
        String response = Downloader.sendGet(m_url, false);

        //Log.i("DownloadTask","run(): got data "+m_type);
        //Log.i("DownloadTask","run(): data >"+response+"<");

        boolean resOk = false;
        
        synchronized (DataCollector.provider()) {
            resOk = DataCollector.provider().parseByType(m_type, m_url, response, m_data);
        }

        if (resOk == true) {
            DataCollector.sendGlobal(BaseProvider.DLOAD_RES, Downloader.LOAD_OK);
        } else {
            Log.i("DownloadTask", "run(): ERROR");
            DataCollector.sendGlobal(BaseProvider.DLOAD_RES, Downloader.LOAD_ERR);
        }
    }
}
