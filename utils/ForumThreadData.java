//
//Copyright (C) 2016-2017 Mikhail Fedotov <anyremote@mail.ru>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA. 
//
package ar.forum.utils;

//import android.os.Parcel;
//import android.os.Parcelable;

import java.io.Serializable;

public class ForumThreadData implements Serializable /*Parcelable*/ {
    public String description; // short description
    public String html;        // data to show (or forum name if used inside UserPostsFragment)
    public String href;        // URL
    public String date;        // date of post
    
    public int    authorIndex;
    public int    answers;     
    public int    level;

    public ForumThreadData () {
        authorIndex = -1;
        answers = 0;
        level = 0;
    }

    /*ForumThreadData(Parcel in) {

        description    = in.readString();
        html           = in.readString();
        href           = in.readString();
        date           = in.readString();
        authorIndex    = in.readInt();
        answers        = in.readInt();
        level          = in.readInt();
    }

    @Override
    public int describeContents() {
       return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(description);
        dest.writeString(html);
        dest.writeString(href);
        dest.writeString(date);
        dest.writeInt(authorIndex);
        dest.writeInt(answers);
        dest.writeInt(level);
    }

    // Creator
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {

        public ForumThreadData createFromParcel(Parcel in) {
            return new ForumThreadData(in);
        }

        public ForumThreadData[] newArray(int size) {
            return new ForumThreadData[size];
        }
    };*/
}
