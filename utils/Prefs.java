//
//Copyright (C) 2016 Mikhail Fedotov <anyremote@mail.ru>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA. 
//

package ar.forum.utils;

import android.app.Activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import ar.forum.BaseFragment;
import ar.forum.DataCollector;

public class Prefs {
    
    public static final String PREFS_FORUMS          = "forums";
    public static final String PREFS_FAVS            = "favs";
    public static final String PREFS_SHORTCUTS       = "shortcuts";
    public static final String PREFS_START_WITH_FAVS = "start_favs";
    public static final String PREFS_USER            = "user";
    public static final String PREFS_PASS            = "p";  
    public static final String PREFS_THEME           = "theme";
    public static final String PREFS_FONT_HEAD       = "fonthsz";
    public static final String PREFS_FONT_POST       = "fontpsz";

    public static final int UNKNOWN     = -1;
 
    public static final int HEADER_TEXT = 0;
    public static final int POST_TEXT   = 1;
   
    boolean prefsSynced        = false;
    public boolean startWithFavorites = false;
    int     themeId            = UNKNOWN;
    String  user               = "";
    String  pass               = "";
    int     textSizeHeaders    = -1;
    int     textSizePosts      = -1;
     
    public void setForumListSynced(boolean v) {
        prefsSynced = v;
    }
    
    public boolean isForumListSynced() {
        return prefsSynced;
    }
    
    public boolean startWithFavs() {
        return startWithFavorites;
    }
      
    public String getUser() {
        return user;
    }
    
    public String getPassword() {
        return pass;
    }
    
    public int getTheme() {
        return themeId;
    }
    
    public int textSize(int type) {
        int sz = (type == HEADER_TEXT ? textSizeHeaders : textSizePosts);
       
        if (sz < BaseFragment.TEXT_MIN_SIZE) {
            sz = BaseFragment.TEXT_MIN_SIZE;
        }
        if (sz > BaseFragment.TEXT_MAX_SIZE) {
            sz = BaseFragment.TEXT_MAX_SIZE;
        }
        return sz;
    }
    
    private String transform(String in) {
        StringBuilder s = new StringBuilder(in);
        s = s.reverse();
        if (s.length() > 3) {
            char c = s.charAt(0);
            s.setCharAt(0,s.charAt(1));
            s.setCharAt(1,c);
            
            c = s.charAt(s.length()-1);
            s.setCharAt(s.length()-1,s.charAt(s.length()-2));
            s.setCharAt(s.length()-2,c);
        }
        return s.toString();        
    }
   
    public void loadPrefs(Activity act, int defaultTheme) {
        
        String[] forums = null;
        String[] favs   = null;
        String[] shcuts = null;
       
        DataCollector.cache().clearForums();
        DataCollector.cache().clearFavorites();
        DataCollector.cache().clearShortcuts();
        
        try {
            SharedPreferences preference = act.getPreferences(Context.MODE_PRIVATE);
            forums = preference.getString(PREFS_FORUMS, "").split("\n"); 
            favs   = preference.getString(PREFS_FAVS, "").split("\n"); 
            shcuts = preference.getString(PREFS_SHORTCUTS, "").split("\n");  
            
            startWithFavorites = (preference.getInt(PREFS_START_WITH_FAVS, 0) == 1);
            user = preference.getString(PREFS_USER, "");
            pass = transform(preference.getString(PREFS_PASS, ""));
            themeId = preference.getInt(PREFS_THEME, defaultTheme);
            textSizeHeaders = preference.getInt(PREFS_FONT_HEAD, defaultTheme);
            textSizePosts   = preference.getInt(PREFS_FONT_POST, defaultTheme);          
        } catch(Exception z) { 
            Log.i("Prefs.loadPrefs", "Exception (2) "+z.getMessage());
        }
        
        decodeForums(forums);
        decodeFavs(favs);
        decodeShortcuts(shcuts);
        
    }       
    
    public void decodeForums(String[] encoded) {
        
       synchronized (DataCollector.cache()) {
            
            for (int j=0;j<encoded.length;j+=4) {
                
                if (j+3 >= encoded.length) {
                    break;
                }
                
                String title = encoded[j].trim();
                String href  = encoded[j+1].trim();
                String level = encoded[j+2];
                String exp   = encoded[j+3];
                
                //Log.i("decodeForums", "decode >"+head+"< >"+titl+"< >"+href+"<");
                
                boolean formatV2 = (level.compareTo("0") == 0 || level.compareTo("1") == 0);
                boolean expanded = (exp.compareTo("e") == 0);
                int intLevel = 0;
                
                if (formatV2) {
                    try {
                        intLevel = Integer.parseInt(level);
                    } catch (Exception e) { 
                        intLevel = 0;
                    }
                } else {
                    if (!title.isEmpty() && level.trim().isEmpty() && href.trim().isEmpty()) {
                        // level 0
                        intLevel = 0;
                    } else {
                        // level 1
                        title = level;
                        intLevel = 1;
                    }
                }

                DataCollector.cache().addForum(title,href,intLevel,expanded);
            }
         }       
     
         //Log.i("decodeForums", "got #"+.DataCollectorcache.forumsSize());
         if (!DataCollector.cache().forumsEmpty()) {
             prefsSynced = true;
         }
    }

    public void decodeFavs(String[] encoded) {
                
        synchronized (DataCollector.cache()) {
           
           for (int j=0;j<encoded.length;j+=2) {
               
               if (j+1 >= encoded.length) {
                   break;
               }
               
               String href = encoded[j];
               String titl = encoded[j+1];
               
               //Log.i("decodeFavs", "decode >"+head+"< >"+titl+"< >"+href+"<");
               if (!titl.trim().isEmpty() && !href.trim().isEmpty()) {
                   DataCollector.cache().addFavorite(titl.trim(),href.trim()); 
               }
           }
        } 
    }
    
    public void decodeShortcuts(String[] encoded) {
        Log.i("Prefs", "decodeShortcuts " + encoded);

        synchronized (DataCollector.cache()) {

            for (int j = 0; j < encoded.length; j += 2) {

                if (j + 1 >= encoded.length) {
                    break;
                }

                String href = encoded[j];
                String titl = encoded[j + 1];

                if (!titl.trim().isEmpty() && !href.trim().isEmpty()) {

                    href = href.trim();

                    int type = DataCollector.provider().decodeShortcutType(href.charAt(0));
                    href = href.substring(1);

                    DataCollector.cache().addShortcut(titl.trim(), href.trim(), type, false);
                }
            }
        }
    }
 
    public void savePrefs(Activity act) {             

        SharedPreferences preference = act.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preference.edit();

        editor.remove(PREFS_FORUMS).commit();

        try {
            String encoded = "";
            synchronized (DataCollector.cache()) {
                for (int i=0;i<DataCollector.cache().forumsSize();i++) {
    
                    ContentItem c = DataCollector.cache().forum(i);
    
                    if (c.title.isEmpty() && c.href.isEmpty()) {
                        continue;
                    }
                    encoded += c.title + "\n" + 
                               c.href.trim() + "\n" + 
                               c.level + "\n" + 
                               (c.expanded ? "e" : "c") + "\n";
                }
                editor.putString(PREFS_FORUMS, encoded);
            }
        } catch(Exception z) { }
        
        editor.commit();
        
        prefsSynced = true;
    }
    
    public void saveFavs(Activity act) {             
       
        SharedPreferences preference = act.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preference.edit();
        
        editor.remove(PREFS_FAVS).commit();

        try {
            String encoded = "";
            synchronized (DataCollector.cache()) {
                for (int i=0;i<DataCollector.cache().favoritesSize();i++) {
    
                    FavoriteItem c = DataCollector.cache().favorite(i);
    
                    if (c.href.isEmpty() || c.title.isEmpty()) {
                        continue;
                    }
                    encoded += c.href.trim() + "\n" + c.title.trim() + "\n";
                }
            }
            editor.putString(PREFS_FAVS, encoded);
        } catch(Exception z) { }

        editor.commit();
    }
    
    public void saveShortcuts(Activity act) {             
        
        SharedPreferences preference = act.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preference.edit();
        
        editor.remove(PREFS_SHORTCUTS).commit();

        try {
            String encoded = "";
            synchronized (DataCollector.cache()) {
                for (int i=0;i<DataCollector.cache().shortcutsSize();i++) {
    
                    ShortcutItem c = DataCollector.cache().shortcut(i);
    
                    if (c.href.isEmpty() || c.title.isEmpty()) {
                        continue;
                    }

                    char type = DataCollector.provider().encodeShortcutType(c.type);

                    encoded += type+c.href.trim() + "\n" + c.title.trim() + "\n";
                }
            }
            editor.putString(PREFS_SHORTCUTS, encoded);
        } catch(Exception z) { }

        editor.commit();
    }
   
    public void saveStartWithFavs(Activity act, int i) {
        
        //Log.i("saveStartWithFavs","#"+i); 
        startWithFavorites = (i == 1);
        
        SharedPreferences preference = act.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preference.edit();
        editor.putInt(PREFS_START_WITH_FAVS, i).commit();
    }
 
    public void saveTheme(Activity act, int i) {
        
        themeId = i;
        
        SharedPreferences preference = act.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preference.edit();
        editor.putInt(PREFS_THEME, i).commit();
    }
    
    public void saveTextSize(Activity act, int type, int s) {
        
        if (s < BaseFragment.TEXT_MIN_SIZE) {
            s = BaseFragment.TEXT_MIN_SIZE;
        }
        if (s > BaseFragment.TEXT_MAX_SIZE) {
            s = BaseFragment.TEXT_MAX_SIZE;
        }
        if (type == HEADER_TEXT) {
            textSizeHeaders = s;
        } else {
            textSizePosts   = s;
        }
        
        if (act != null) {
            String key = (type == HEADER_TEXT ? PREFS_FONT_HEAD : PREFS_FONT_POST);
            SharedPreferences preference = act.getPreferences(Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preference.edit();
            editor.putInt(key, s).commit();
        }
    }

    public boolean saveUser(Activity act, String u, String p) {
        SharedPreferences preference = act.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preference.edit();
        
        String user = preference.getString(PREFS_USER, "");
        String pass = transform(preference.getString(PREFS_PASS, ""));
        
        if (user.isEmpty() || 
            pass.isEmpty() ||
            user.compareTo(u) != 0 ||
            pass.compareTo(p) != 0) {
            
            this.user = u;
            this.pass = p;
            
            editor.putString(PREFS_USER, u).putString(PREFS_PASS, transform(p)).commit();
            return true;
        }
        return false;
    }
    
    public void deletePrefs(Activity act) {
        
        SharedPreferences preference = act.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preference.edit();
        
        editor.remove(PREFS_FAVS)
              .remove(PREFS_SHORTCUTS)
              .remove(PREFS_FORUMS)
              .remove(PREFS_START_WITH_FAVS)
              .remove(PREFS_USER)
              .remove(PREFS_PASS)
              .remove(PREFS_THEME)
              .remove(PREFS_FONT_HEAD)
              .remove(PREFS_FONT_POST)
              .commit();
    }
}
