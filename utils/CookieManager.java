//
// Copyright (C) 2016 Mikhail Fedotov <anyremote@mail.ru>
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA. 
//

package ar.forum.utils;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;


import android.util.Log;
import android.util.Pair;
import ar.forum.DataCollector;

public class CookieManager {
    
    static final String COOKIES_HEADER = "Set-Cookie";
    
    static private Map<String,String> m_CookieManager = null;
    
    public CookieManager() {
        if (m_CookieManager == null) {
            m_CookieManager = new HashMap<String,String>();
        }
    }

    public void clear() {
        //Log.i("cleanCookies", "");
        m_CookieManager.clear();
    }
    
    public void add(String key, String value) {
        m_CookieManager.put(key, value);
    }

    public void remove(String key) {
        m_CookieManager.remove(key);
    }
    
    public int size() {
        return m_CookieManager.size();
    }
    
    public String getCookies() { 
        //Log.i("getCookies", "stored cookies #" + m_CookieManager.size());        
        StringBuilder cookieData = new StringBuilder();

        for (Map.Entry<String,String> entry : m_CookieManager.entrySet()) {
            
             if (cookieData.length() != 0) {
                 cookieData.append("; ");
             }
             //Log.i("getCookies:", entry.getKey()+" -> "+entry.getValue());
             String cookie = entry.getKey() + "=" + entry.getValue();    
             cookieData.append(cookie);
        }
        //Log.i("getCookies", "COOKIES: " + cookieData.toString());       
        return cookieData.toString();
    }
    
    public void dumpCookies() { 
        for (Map.Entry<String,String> entry : m_CookieManager.entrySet()) {
             Log.i("getCookies:", entry.getKey()+" -> "+entry.getValue());
        }
    }
   
    public String getTunedCookies(Vector<Pair<String,String> > overrideList) { 
        //Log.i("getCookies", "stored cookies #" + m_CookieManager.size());        
        StringBuilder cookieData = new StringBuilder();

        for (Map.Entry<String,String> entry : m_CookieManager.entrySet()) {
            
             if (cookieData.length() != 0) {
                 cookieData.append("; ");
             }
             
             String k = entry.getKey();
             String v = entry.getValue();
             //String cookie = entry.getKey() + "=" + entry.getValue();    
             for (Pair<String,String> override : overrideList) {
                  if (override.first.compareTo(k) == 0) {
                      v = override.second;
                  }
             }
             String cookie = k + "=" + v; 
             cookieData.append(cookie);
        }
        //Log.i("getCookies", "COOKIES: " + cookieData.toString());       
        return cookieData.toString();
    }
    
    // Store (add) cookies
    public void storeCookies(HttpURLConnection conn) {
         
        Map<String, List<String>> headerFields = conn.getHeaderFields();
        List<String> cookiesHeader = headerFields.get(COOKIES_HEADER);
        
        String adomain = DataCollector.provider().cookieAcceptDomain();
        
        String acceptDomain = "omain=" + adomain;  // can be domain or Domain, so skip 1st letter
        
        Map<String,String> processed = new HashMap<String,String>();
        
        if (cookiesHeader != null) {
            for (String cookieStr : cookiesHeader) {
                //Log.i("storeCookie", "cookie str: "+cookieStr);
                
                int idx = cookieStr.indexOf('=');
                if (idx <= 0) {
                     //Log.i("storeCookie", "skip " + cookieStr);              
                     continue;
                }
              
                if (adomain.length() > 0) {
                    int i2 = cookieStr.indexOf(acceptDomain);
                    if (i2 < 0) {
                        //Log.i("storeCookie", "skip by domain " + cookieStr);              
                        continue;
                    }
                }
                String key   = cookieStr.substring(0, idx);
                String value = cookieStr.substring(idx+1);
                
               // Log.i("storeCookie", "cookie parsed: "+key+" -> "+value);
                if (value.startsWith("deleted;")) {
                    //Log.i("storeCookie", "remove cookie: "+key);
                    if (!processed.containsKey(key)) {   // several cookies in one response, only one with "deleted" value
                         remove(key);
                    }
                } else {
                    //Log.i("storeCookie", "add cookie: "+key);
                    add(key, value);
                    processed.put(key, "");
                }
            } 
            //Log.i("storeCookie", "stored cookies #" + size());              
        }
    }
}
