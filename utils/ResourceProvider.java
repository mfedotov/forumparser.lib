//
// Copyright (C) 2016 Mikhail Fedotov <anyremote@mail.ru>
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA. 
//

package ar.forum.utils;

public interface ResourceProvider {  
    // Common
    public static final int APP_NAME            = 1;
    public static final int APP_ICON            = 2;
    public static final int LANGUAGE            = 3;
    public static final int USER                = 4;
    public static final int DATE                = 5;
    public static final int RELOAD              = 10;
    public static final int ANSWERS             = 11;
    public static final int USER_ICON           = 12;
    public static final int USER_DRAW_ICON      = 13;
    public static final int ADD_FAVS            = 14;   
    public static final int ADD_SHORTCUT        = 15;
    public static final int CANCEL              = 16;

    public static final int FEED                = 21;

    // Splash
    public static final int LAYOUT_SPLASH       = 31;
    
    // About
    public static final int LAYOUT_ABOUT        = 41;
    public static final int VIEW_ABOUT          = 42;
    public static final int LEGAL_TEXT          = 43;
    public static final int INFO_TEXT           = 44;
    public static final int RAW_LEGAL           = 45;
    public static final int RAW_INFO            = 46;             

    // Headers + Favorites + Shortcuts
    public static final int LAYOUT_HEADERS      = 51;             
    public static final int HEADER_LIST         = 52;
    public static final int HEADER_LIST_ITEM    = 53;
    public static final int HEADER_TEXT         = 54;
    public static final int HEADER_CTX_MENU     = 55;
    public static final int HEADER_CTX_MENU2    = 56;
    public static final int EXPAND_ICON         = 58;             
    public static final int ICON_UP             = 59;             
    public static final int ICON_DOWN           = 60;
    
    public static final int FAVS                = 71;                          
    public static final int REMOVE_FAVS         = 72;
    public static final int FAVS_CTX_MENU       = 73;  
    public static final int SHORTCUTS           = 74;
    public static final int SHORTCUT_CTX_MENU   = 75;  
    public static final int REMOVE_SHORTCUT     = 76;           
     
    // Threads + Messages
    public static final int LAYOUT_THREADS      = 81;
    public static final int THREAD_LIST         = 82;
    public static final int THREAD_LIST_ITEM    = 83;
    public static final int THREAD_LIST_ITEM_EX = 84;
    public static final int THREAD_TITLE        = 85;
    public static final int THREAD_ROW          = 86;
    public static final int THREAD_ITEM         = 87;
    public static final int THREAD_HTML         = 88;
    public static final int UPOST_LIST_ITEM     = 89;
    public static final int UPOST_ITEM          = 90;
    
    // Web view
    public static final int LAYOUT_WEB          = 91;
    
    public static final int FORUM_CTX_MENU      = 101;
    public static final int FORUM_CTX_MENU_EX   = 102;
    public static final int THREAD_CTX_MENU     = 103;
    public static final int THREAD_CTX_MENU_EX  = 104;
    public static final int POST_SEND           = 105;
    
    public static final int USER_MSGS           = 111;
    public static final int MESSAGE             = 112;
    public static final int MSG_CTX_MENU        = 113;
    public static final int ANSWER_SEND         = 114;

    // New Message
    public static final int VIEW_NEWMSG         = 121;
    public static final int LAYOUT_NEWMSG       = 122;
    public static final int SUBJ_LABEL          = 123;
    public static final int SUBJ_EDIT           = 124;
    public static final int MSG_LABEL           = 125;
    public static final int MSG_EDIT            = 126;
    public static final int QUOTE               = 127;
    public static final int MSG_SEND            = 128;
    public static final int NEW_MSG             = 129;

    // User View
    public static final int LAYOUT_USER         = 141;
    public static final int VIEW_WEB            = 142;
    public static final int PROFILE             = 143;
    public static final int AVATAR              = 144;
    public static final int NICK                = 145;
    public static final int B_ALL_MSGS          = 146;
    public static final int CUR_USER            = 147;
    
    // Messages
    public static final int PRESS_TO_EXIT       = 151;
    public static final int DLOAD_ERR           = 152;
    public static final int LOGIN_ERR           = 153;
    public static final int NEWMSG_ERR          = 154;
    public static final int WRONG_PASS          = 155;
    
    // 
    public static final int USER_MENU           = 161;
    public static final int USER_PROFILE        = 162;
    public static final int USER_MESSAGES       = 163;

    // Search
    public static final int SEARCH_LAYOUT       = 171;
    public static final int SEARCH_TEXT         = 172;
    public static final int SEARCH              = 173;

    public int resourceId(int id);
}
