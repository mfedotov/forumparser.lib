//
//Copyright (C) 2016 Mikhail Fedotov <anyremote@mail.ru>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA. 
//

package ar.forum.utils;

import java.util.Vector;

import ar.forum.DataCollector;


public interface ContentProvider {
    
    public String name();

    public String topURL();

    public String topForumsURL();
    public String forumURL(String href, int page);       
    public String threadURL(String href, int page);
    public boolean isThreadMultiPaged();

    public String urlForType(int type, int subtype, String url, int page);

    public String loginURL();
    public String profileURL(String suffix);  // empty suffix means "current user"
    public String authorMessagesURL(Author a, int page);

    public String messagesURL();
    public String messageURL(String suffix);
    public String sendMessageURL();
    public String formDataUser();
    public String formDataPass();
    public String cookieAcceptDomain();
    public String encoding();
    public String newMsgURL(String v);
    
    public boolean iconInHeaders();
    public boolean shortcutsOnly();
    public boolean supportUserMessagesList();

    public int decodeShortcutType(char c);
    public char encodeShortcutType(int t);

    public boolean parseByType(int type, String url, String response, int data); // to be used in inherited providers

    public boolean parseHeaders     (String input);
    public boolean parseForum       (String input, boolean first);
    public boolean parseThread      (String input, boolean first);
    public boolean parsePost        (String url, String input);
    public int     parseUser        (String url, String input);
    public boolean parseProfile     (String input); 
    public boolean parsePrepareMsg  (String input); 
    public boolean parseMessages    (String input);
    public boolean parseMessage     (String input);
    public boolean parseUserMessages(String input, int uid, boolean firstPage);

    public String threadCSS();  // CSS to modify view
   
    public Vector<ForumThreadData> msgList();
    public ForumThreadData msg(int i);
    
    public Vector<ForumThreadData> msgThread();
    
    public Vector<ForumThreadData> forumData();
    public ForumThreadData forumData(int i);

    public Vector<ForumThreadData> threadData(); 
    public ForumThreadData threadData(int i);
    
    public Vector<ForumThreadData> userMessages();
}
