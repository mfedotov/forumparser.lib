//
//Copyright (C) 2016-2017 Mikhail Fedotov <anyremote@mail.ru>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA. 
//

package ar.forum.utils;

//import android.os.Parcel;
//import android.os.Parcelable;

import java.io.Serializable;

public class FavoriteItem implements Serializable /*Parcelable*/ {
    public String  title;
    public String  href;

    public FavoriteItem() {
    }

    /*FavoriteItem(Parcel in) {
        title          = in.readString();
        href           = in.readString();
    }

    @Override
    public int describeContents() {
       return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(href);
    }

    // Creator
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {

        public FavoriteItem createFromParcel(Parcel in) {
            return new FavoriteItem(in);
        }

        public FavoriteItem[] newArray(int size) {
            return new FavoriteItem[size];
        }
    };*/
}
