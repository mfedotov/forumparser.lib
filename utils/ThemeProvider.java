//
//Copyright (C) 2016 Mikhail Fedotov <anyremote@mail.ru>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA. 
//

package ar.forum.utils;

public interface ThemeProvider {
    
    public static final int THEME_LIGHT       = 1;
    public static final int THEME_DARK        = 2;
    public static final int THEME_ORIGINAL    = 3;
    
    public int colorBkgr       (int theme);  // general background
    public int colorBkgrSplash (int theme);  // background of splash screen
    public int colorItemBkgr   (int theme);  // background of list item
    public int colorDivider    (int theme);  // divider between items (if exists)
    public int colorFrame      (int theme);  // rounded frame color around list item + item background
    public int colorText       (int theme);  // main text color
    public int colorTextGrayed (int theme);  // grayed text colot
}
