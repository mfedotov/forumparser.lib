//
//Copyright (C) 2017 Mikhail Fedotov <anyremote@mail.ru>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
//

package ar.forum.utils;

public class Feed {

    public Feed(int id) {
        m_id          = id;
        m_curCategory = "";
        resetPage();
    }

    public Feed(int id, String v) {
        m_id          = id;
        m_curCategory = v;
        resetPage();
    }

    public int id() {
        return m_id;
    }

    public void setCategory(String v) {
        m_curCategory = v;
    }

    public String category() {
        return m_curCategory;
    }

    public void resetPage() {
        m_curPage = 1;
    }

    public int incrPage() {
        m_curPage++;
        return m_curPage;
    }

    int    m_id;
    int    m_curPage;
    String m_curCategory;
}
