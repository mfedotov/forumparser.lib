//
//Copyright (C) 2016-2017 Mikhail Fedotov <anyremote@mail.ru>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA. 
//

package ar.forum;

import java.util.ArrayList;
import java.util.Vector;

import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import ar.forum.utils.ForumThreadData;
import ar.forum.utils.Prefs;
import ar.forum.utils.ResourceProvider;

public class UserPostsFragment extends BaseFragment 
                                       implements OnItemClickListener {
        
        private ListView     m_list = null;
        private ForumAdapter m_dataSource;
        private Parcelable m_listSavedState = null;
        private int          m_uid;

        public UserPostsFragment() {
            super();
            m_uid = -1;
         }
        
        public void setup(int u) {
            m_uid = u;
         }
        
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            
            Log.i("UserPostsFragment","onCreateView");
     
            DataCollector.setStage(BaseProvider.SHOW_USER_MSGS);
            
            View rootView = inflater.inflate(DataCollector.resourceId(ResourceProvider.LAYOUT_THREADS), container, false); 
            
            m_list = (ListView) rootView.findViewById(DataCollector.resourceId(ResourceProvider.THREAD_LIST));
            m_list.setBackgroundResource(DataCollector.colorBkgr());
            m_list.setDivider(getResources().getDrawable(DataCollector.colorBkgr()));
            m_list.setDividerHeight(3);
            m_list.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

            onCreateBaseView(m_list); // set scroll counters
            
            createAdapter();

            //list.setOnItemSelectedListener(this);
            m_list.setOnItemClickListener(this);

            return rootView;
        }

        @Override
        public void onSaveInstanceState(final Bundle outState) {
            Log.i("UserPostsFragment", "onSaveInstanceState");
            super.onSaveInstanceState(outState);

            if (m_list != null) {
                m_listSavedState = m_list.onSaveInstanceState();
                outState.putParcelable("userposts", m_listSavedState);
            }
        }
        
        @Override
        public void onActivityCreated (Bundle savedInstanceState) {
            Log.i("UserPostsFragment","onActivityCreated");

            if (savedInstanceState != null) {
                Log.i("UserPostsFragment", "onActivityCreated have saved state");
                //probably orientation change
                createAdapter();
                m_listSavedState = savedInstanceState.getParcelable("userposts");

            } else if (m_dataSource != null) {
                //returning from backstack, data is fine, do nothing
            } else {
                //newly created, compute data
                createAdapter();
            }

            if (m_list != null) {
                if (m_listSavedState != null) {
                    m_list.onRestoreInstanceState(m_listSavedState);
                }
                registerForContextMenu(m_list);
            }
            setHasOptionsMenu(true);        
            super.onActivityCreated(savedInstanceState);
        }

       @Override
        public void onStart() {
            Log.i("UserPostsFragment", "onStart");
            super.onStart();
            setTitle(getResources().getString(DataCollector.resourceId(ResourceProvider.USER_MSGS)));
        }
        public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
            final ForumThreadData item = m_dataSource.get(arg2);
            Log.i("UserPostsFragment","onItemClick "+item.authorIndex+" >"+item.href+"<"); 
            if (item != null && !item.href.isEmpty()) {
                DataCollector.sendGlobal(BaseProvider.LOAD_THREAD, item.href);
            }
        }
        
        @Override
        public void onCreateContextMenu (ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            Log.i("UserPostsFragment","onCreateContextMenu"); 
            
            int mid = v.getId();
            if (mid == DataCollector.resourceId(ResourceProvider.THREAD_LIST)) {

                DataCollector.setMenuByFragmentId(getID());

                menu.clear();
              
                final AdapterContextMenuInfo info = (AdapterContextMenuInfo) menuInfo;
                int idx = info.position; 
                
                ForumThreadData ci = m_dataSource.get(idx);
                if (ci == null) {
                    return;
                }
                
                //menu.setHeaderIcon(getResources().getDrawable(R.drawable.ic_fav));
                final TextView tv = (TextView) info.targetView.findViewById(DataCollector.resourceId(ResourceProvider.THREAD_TITLE));
                if (tv != null) {
                    menu.setHeaderTitle(tv.getText());
                }
               
                MenuInflater inflater = getActivity().getMenuInflater();
                inflater.inflate(DataCollector.resourceId(ResourceProvider.FORUM_CTX_MENU), menu);
            }
        }

        // Handle context menu, opened by long-click
        @Override
        public boolean onContextItemSelected(MenuItem item) {

            if (DataCollector.menuByFragmentId() != getID()) {
                return false;
            }
            if( getUserVisibleHint() == false ) {
                return false;
            }

            final AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
            
            int idx = info.position;
            Log.i("UserPostsFragment","onContextItemSelected "+info.position+" "+item.getItemId()); 
            
            int mid = item.getItemId();

            if (mid == DataCollector.resourceId(ResourceProvider.ADD_SHORTCUT)) {

                if (idx >= 0) { 
                    final ForumThreadData ci = m_dataSource.get(idx);
                    if (ci != null) {
                        Log.i("UserPostsFragment","onContextItemSelected #"+ci.href);
                        DataCollector.cache().addShortcut(ci.description, ci.href, BaseProvider.LOAD_FORUM, true);
                    }
                }
                return true;
            } else if (mid == DataCollector.resourceId(ResourceProvider.POST_SEND)) {   
     
                  if (idx >= 0) { 
                       final ForumThreadData ft = m_dataSource.get(idx);
                       if (ft != null) {
                           DataCollector.sendGlobal(BaseProvider.PREPARE_POST, ft);
                       }
                  }
                  return true;
             }
            
            return super.onContextItemSelected(item);
        }
        
        public void createAdapter() {

            ArrayList<ForumThreadData> listItems = new ArrayList<ForumThreadData>();

            synchronized (DataCollector.provider()) {
                Vector<ForumThreadData> ff = DataCollector.provider().userMessages();
                for (int i = 0; i < ff.size(); i++) {
                    listItems.add(ff.get(i));
                }
            }

            Log.i("UserPostsFragment","createAdapter #"+listItems.size());
            m_dataSource = new ForumAdapter(getActivity(),
                                                DataCollector.resourceId(ResourceProvider.THREAD_LIST_ITEM), 
                                                ForumAdapter.ADAPT_USERMSGS, listItems);
            if (m_list != null){
                m_list.setAdapter(m_dataSource);
            }
        }
        
        public void update() {
            Log.i("UserPostsFragment","update"); 
            m_dataSource.update();
        }
        
        public BaseAdapter adapter() {
            return m_dataSource;
        }

        public void onZoom(int zoom) {   
            onZoom(zoom, Prefs.POST_TEXT);
        }

        public void onOverFling(int dir) {
            Log.i("UserPostsFragment","onOverFling "+dir); 
            if (dir == BaseFragment.SWIPE_DOWN) {
                Log.i("UserPostsFragment","NEED UPDATE"); 
                DataCollector.sendGlobal(BaseProvider.LOAD_USER_MSGS_AGAIN, m_uid);
            } else if (dir == BaseFragment.SWIPE_UP) {
                Log.i("UserPostsFragment","NEED NEXT PORTION"); 
                DataCollector.sendGlobal(BaseProvider.LOAD_USER_MSGS_NEXT, m_uid);
            }
        }
}
