//
// Copyright (C) 2017 Mikhail Fedotov <anyremote@mail.ru>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//

package ar.forum;

import android.util.Log;

import java.util.Vector;

import ar.forum.utils.Author;
import ar.forum.utils.ContentProvider;
import ar.forum.utils.ForumThreadData;

public class BaseProvider implements ContentProvider {

    public static final int UNDEFINED            = 0;

    public static final int SHOW_SPLASH          = 2;
    public static final int LOGIN                = 3;
    public static final int LOGIN_OK             = 4;
    public static final int LOGIN_ERR            = 5;
    public static final int SHOW_FAVS            = 6;
    public static final int SHOW_SHORTCUTS       = 7;
    public static final int UPDATE_VISIBLE       = 8;

    public static final int LOAD_FORUMS          = 11;
    public static final int SHOW_FORUMS          = 12;

    public static final int LOAD_FORUM           = 15;
    public static final int LOAD_FORUM_NEXT      = 16;
    public static final int LOAD_FORUM_AGAIN     = 17;
    public static final int SHOW_FORUM           = 18;

    public static final int LOAD_THREAD          = 21;
    public static final int LOAD_THREAD_NEXT     = 22;
    public static final int LOAD_THREAD_AGAIN    = 23;
    public static final int SHOW_THREAD          = 24;

    public static final int LOAD_SEARCH          = 26;
    public static final int LOAD_SHORTCUT        = 27;

    public static final int LOAD_POST            = 31;
    public static final int SHOW_POST            = 32;

    public static final int LOAD_USER_INFO       = 53;
    public static final int SHOW_USER_INFO       = 54;
    public static final int CURUSER_INFO         = 55;
    public static final int CURUSER_READY        = 56;
    public static final int PREPARE_POST         = 57;
    public static final int SEND_POST            = 58;
    public static final int SEND_POST_ERR        = 59;

    public static final int LOAD_MSGS            = 60;
    public static final int LOAD_MSGS_AGAIN      = 61;
    public static final int SHOW_MSGS            = 62;
    public static final int LOAD_MSG             = 65;
    public static final int SHOW_MSG             = 66;

    public static final int LOAD_USER_MSGS       = 70;
    public static final int LOAD_USER_MSGS_NEXT  = 71;
    public static final int LOAD_USER_MSGS_AGAIN = 72;
    public static final int SHOW_USER_MSGS       = 73;

    public static final int SHOW_WEB             = 80;
    public static final int SHOW_WEB_INTERNAL    = 81;
    public static final int SHOW_WEB_EXTERNAL    = 82;

    public static final int PREPARE_MESSAGE      = 85;
    public static final int SEND_MESSAGE         = 86;
    public static final int SEND_MESSAGE_ERR     = 87;
    public static final int SHOW_SETUP           = 88;

    public static final int SETUP_THEME          = 91;
    public static final int SETUP_HEADER_SIZE    = 92;
    public static final int SETUP_POST_SIZE      = 93;
    public static final int SETUP_USER           = 94;
    public static final int SETUP_OPENFAVS       = 95;
    public static final int SAVE_SHORTCUTS       = 96;
    public static final int SAVE_FAVS            = 97;

    public static final int DELETE_ALL           = 98;
    public static final int DLOAD_RES            = 99;

    public static final int CODE_COMMON_MAX      = 200;  // codes above to be used in child classes

    protected Vector<ForumThreadData> forumData     = new Vector<ForumThreadData>();
    protected Vector<ForumThreadData> threadData    = new Vector<ForumThreadData>();
    protected Vector<ForumThreadData> messages      = new Vector<ForumThreadData>();
    protected Vector<ForumThreadData> messageThread = new Vector<ForumThreadData>();
    protected Vector<ForumThreadData> userMessages  = new Vector<ForumThreadData>();

    @Override
    public String name() {
        return "";
    }

    @Override
    public String urlForType(int type, int subtype, String url, int page) {
        return "";
    };

    @Override
    public String topURL() {
        return "";
    }

    @Override
    public String topForumsURL() {
        return "";
    }

    @Override
    public String forumURL(String href, int page) {
        return "";
    }

    @Override
    public String threadURL(String href, int page) {
        return "";
    }

    @Override
    public boolean isThreadMultiPaged() {
        return false;
    }

    @Override
    public String loginURL() {
        return "";
    }

    @Override
    public String profileURL(String suffix) {
        return "";
    }

    @Override
    public String authorMessagesURL(Author a, int page) {
        return "";
    }

    @Override
    public String messagesURL() {
        return "";
    }

    @Override
    public String messageURL(String suffix) {
        return "";
    }

    @Override
    public String sendMessageURL() {
        return "";
    }

    @Override
    public String formDataUser() {
        return "";
    }

    @Override
    public String formDataPass() {
        return "";
    }

    @Override
    public String cookieAcceptDomain() {
        return "";
    }

    @Override
    public String encoding() {
        return "";
    }

    @Override
    public String newMsgURL(String v) {
        return "";
    }

    @Override
    public boolean iconInHeaders() {
        return false;
    }

    @Override
    public boolean shortcutsOnly() {
        return false;
    }

    public boolean supportUserMessagesList() {return true;}

    @Override
    public int decodeShortcutType(char c) {
        int type = BaseProvider.LOAD_FORUM;
        if (c == 'F') {
            type = BaseProvider.LOAD_FORUM;
        } else if (c == 'T') {
            type = BaseProvider.LOAD_THREAD;
        }
        return type;
    }

    @Override
    public char encodeShortcutType(int type) {
        char c = 'F';
        if (type == BaseProvider.LOAD_FORUM) {
            c = 'F';
        } else if (type == BaseProvider.LOAD_THREAD) {
            c = 'T';
        }
        return c;
    }

    @Override
    public boolean parseHeaders(String input) {
        return false;
    }

    @Override
    public boolean parseForum(String input, boolean first) {
        return false;
    }

    @Override
    public boolean parseThread(String input, boolean first) {
        return false;
    }

    @Override
    public boolean parsePost(String url, String input) {
        return false;
    }

    @Override
    public int parseUser(String url, String input) {
        return 0;
    }

    @Override
    public boolean parseProfile(String input) {
        return false;
    }

    @Override
    public boolean parsePrepareMsg(String input) {
        return false;
    }

    @Override
    public boolean parseMessages(String input) {
        return false;
    }

    @Override
    public boolean parseMessage(String input) {
        return false;
    }

    @Override
    public boolean parseUserMessages(String input, int uid, boolean firstPage) {
        return false;
    }

    @Override
    public String threadCSS() {
        return "";
    }

    @Override
    public Vector<ForumThreadData> msgList() {
        return messages;
    }

    @Override
    public ForumThreadData msg(int i) {
        if (i >= 0 && i < messages.size()) {
            return messages.elementAt(i);
        }
        return null;
    }

    @Override
    public Vector<ForumThreadData> msgThread() {
        return messageThread;
    }

    @Override
    public Vector<ForumThreadData> forumData() {
        return forumData;
    }

    @Override
    public ForumThreadData forumData(int i) {
        if (i >= 0 && i < forumData.size()) {
            return forumData.elementAt(i);
        }
        return null;
    }

    @Override
    public Vector<ForumThreadData> threadData() {
        return threadData;
    }

    @Override
    public ForumThreadData threadData(int i) {
        if (i >= 0 && i < threadData.size()) {
            return threadData.elementAt(i);
        }
        return null;
    }

    @Override
    public Vector<ForumThreadData> userMessages() {
        return userMessages;
    }

    public boolean parseByType(int type, String url, String response, int data) {

        boolean resOk = false;

        try {
            int outMsg = UNDEFINED;

            if (type == LOAD_FORUMS) {
                resOk = parseHeaders(response);
                outMsg = SHOW_FORUMS;

            } else if (type == LOAD_FORUM) {
                resOk = parseForum(response, true);
                outMsg = SHOW_FORUM;

            } else if (type == LOAD_FORUM_AGAIN) {
                resOk = parseForum(response, true);
                outMsg = UPDATE_VISIBLE;

            } else if (type == LOAD_FORUM_NEXT) {
                resOk = parseForum(response, false);
                outMsg = UPDATE_VISIBLE;

            } else if (type == LOAD_THREAD) {
                resOk = parseThread(response, true);
                outMsg = SHOW_THREAD;

            } else if (type == LOAD_THREAD_AGAIN) {
                resOk = parseThread(response, true);
                outMsg = UPDATE_VISIBLE;

            } else if (type == LOAD_THREAD_NEXT) {
                resOk = parseThread(response, false);
                outMsg = UPDATE_VISIBLE;

            } else if (type == LOAD_POST) {
                resOk = parsePost(url, response);
                outMsg = SHOW_POST;

            } else if (type == LOAD_USER_INFO) {
                int idx = parseUser(url, response);
                if (idx >= 0) {
                    resOk = true;
                    outMsg = SHOW_USER_INFO;
                    data = idx;
                }

            } else if (type == CURUSER_INFO) {
                resOk = parseProfile(response);
                outMsg = CURUSER_READY;

            } else if (type == PREPARE_MESSAGE) {
                Log.i("BaseProvider ", "parseByType(): TODO WRITE_MSG_THREAD");
                resOk = parsePrepareMsg(response);

            } else if (type == LOAD_MSGS) {
                resOk = parseMessages(response);
                outMsg = SHOW_MSGS;

            } else if (type == LOAD_MSGS_AGAIN) {
                resOk = parseMessages(response);
                outMsg = UPDATE_VISIBLE;

            } else if (type == LOAD_MSG) {
                resOk = parseMessage(response);
                outMsg = SHOW_MSG;

            } else if (type == LOAD_USER_MSGS) {
                resOk = parseUserMessages(response, data, true);
                outMsg = SHOW_USER_MSGS;

            } else if (type == LOAD_USER_MSGS_AGAIN) {
                resOk = parseUserMessages(response, data, true);
                outMsg = UPDATE_VISIBLE;

            } else if (type == LOAD_USER_MSGS_NEXT) {
                resOk = parseUserMessages(response, data, false);
                outMsg = UPDATE_VISIBLE;
            }

            if (outMsg == UNDEFINED) {
                resOk = false;
            }

            if (resOk) {
                if (type == LOAD_POST) {
                    DataCollector.sendGlobal(outMsg, url);
                } else {
                    DataCollector.sendGlobal(outMsg, data);
                }
            }

        } catch (Exception e) {
            Log.i("BaseProvider", "parseByType(): exception on parse response " + e.getMessage());
        }
        return resOk;
    }
}
