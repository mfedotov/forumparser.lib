//
//Copyright (C) 2016-2017 Mikhail Fedotov <anyremote@mail.ru>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA. 
//

package ar.forum;

import java.util.ArrayList;

import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import ar.forum.utils.ContentItem;
import ar.forum.utils.FavoriteItem;
import ar.forum.utils.Prefs;
import ar.forum.utils.ResourceProvider;

public class FavoritesFragment extends BaseFragment 
                             implements OnItemClickListener {
    
    public static final String NAME = "NAME";
   
    ListView         m_list = null;
    FavoritesAdapter m_dataSource = null;
    private Parcelable m_listSavedState = null;
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
       
        Log.i("FavoritesFragment","onCreateView");

        DataCollector.setStage(BaseProvider.SHOW_FAVS);
        
        View rootView = inflater.inflate(DataCollector.resourceId(ResourceProvider.LAYOUT_HEADERS), container, false);
        
        m_list = (ListView) rootView.findViewById(DataCollector.resourceId(ResourceProvider.HEADER_LIST));
        m_list.setBackgroundResource(DataCollector.colorBkgr());
        m_list.setDivider(getResources().getDrawable(DataCollector.colorDivider()));
        m_list.setDividerHeight(1);
        
        createAdapter();

        m_list.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        m_list.setOnItemClickListener(this);

        return rootView;
    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        Log.i("FavoritesFragment", "onSaveInstanceState");
        super.onSaveInstanceState(outState);

        if (m_list != null) {
            m_listSavedState = m_list.onSaveInstanceState();
            outState.putParcelable("favorites", m_listSavedState);
        }
    }

    @Override
    public void onActivityCreated (Bundle savedInstanceState) {
        Log.i("FavoritesFragment","onActivityCreated");
        if (savedInstanceState != null) {
            Log.i("FavoritesFragment","onActivityCreated have saved state");
            //probably orientation change
            createAdapter();
            m_listSavedState = savedInstanceState.getParcelable("favorites");
        } else if (m_dataSource != null) {
            //returning from backstack, data is fine, do nothing
        } else {
            //newly created, compute data
            createAdapter();
        }

        if (m_list != null) {
            if (m_listSavedState != null) {
                m_list.onRestoreInstanceState(m_listSavedState);
            }
            registerForContextMenu(m_list);
        }
        setHasOptionsMenu(true);        
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        Log.i("FavoritesFragment", "onStart");
        super.onStart();
        onResumeFragment();
    }

    @Override
    public void onResumeFragment() {
        Log.i("FavoritesFragment", "onResumeFragment");
        setTitle(getResources().getString(DataCollector.resourceId(ResourceProvider.FAVS)));
    }

    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
        
        //Log.i("FavoritesFragment","onItemClick"); 
        if (m_dataSource == null) {
            return;
        }

        final FavoriteItem item = m_dataSource.get(arg2);
        if (item != null && !item.href.isEmpty()) {
            
            synchronized (DataCollector.cache()) {
                for (int i=0;i<DataCollector.cache().forumsSize();i++) {
                    ContentItem itm = DataCollector.cache().forum(i); 
                    
                    //Log.i("FavoritesFragment","onItemClick >"+itm.href+"< >"+item.href+"<"); 
                  
                    if (item.href.compareTo(itm.href) == 0) {   // translate from favorites index to headers index
                        //Log.i("FavoritesFragment","onItemClick OK"); 
                        DataCollector.sendGlobal(BaseProvider.LOAD_FORUM, i);
                        return;
                    }
                }
            }
        }
    }
     
    @Override
    public void onCreateContextMenu (ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        Log.i("FavoritesFragment","onCreateContextMenu");

        if (v.getId() == DataCollector.resourceId(ResourceProvider.HEADER_LIST)) {

            DataCollector.setMenuByFragmentId(getID());

            menu.clear();
          
            final AdapterContextMenuInfo info = (AdapterContextMenuInfo)menuInfo;
            int idx = info.position;
           
            if (idx>=0 && idx<DataCollector.cache().forumsSize()) {
                
                ContentItem c = DataCollector.cache().forum(idx);
                final TextView tv= (TextView) info.targetView.findViewById(DataCollector.resourceId(ResourceProvider.HEADER_TEXT));
                menu.setHeaderTitle(tv.getText());
                    
                MenuInflater inflater = getActivity().getMenuInflater();
                inflater.inflate(DataCollector.resourceId(ResourceProvider.FAVS_CTX_MENU), menu);
            }
        }
    }

    // Handle context menu, opened by long-click
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (DataCollector.menuByFragmentId() != getID()) {
            return false;
        }

        if( getUserVisibleHint() == false ) {
            return false;
        }

        final AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
        
        int idx = info.position;
        //Log.i("FavoritesFragment","onContextItemSelected "+info.position+" "+item.getItemId()); 
        
        int iid = item.getItemId();

        if (iid == DataCollector.resourceId(ResourceProvider.REMOVE_FAVS)) {
            if (idx >= 0) {
                //Log.i("FavoritesFragment","onContextItemSelected #"+idx);
                DataCollector.cache().removeFavorite(idx);
                m_dataSource.remove(idx);
            }
            return true;
        }
        
        return super.onContextItemSelected(item);
    }

    private void createAdapter() {

        ArrayList<FavoriteItem> listItems = new ArrayList<FavoriteItem>();

        synchronized (DataCollector.cache()) {
            for (int i = 0; i < DataCollector.cache().favoritesSize(); i++) {
                listItems.add(DataCollector.cache().favorite(i));
            }
        }

        Log.i("FavoritesFragment", "createAdapter #" + listItems.size());
        m_dataSource = new FavoritesAdapter(getActivity(),
                                            DataCollector.resourceId(ResourceProvider.HEADER_LIST_ITEM), listItems);
        if (m_list != null) {
            m_list.setAdapter(m_dataSource);
        }
    }

    public BaseAdapter adapter() {
        return m_dataSource;
    }
    
    public void onZoom(int zoom) {   
        onZoom(zoom, Prefs.HEADER_TEXT);
    }
}
