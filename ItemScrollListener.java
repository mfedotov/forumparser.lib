//
//Copyright (C) 2015 Mikhail Fedotov <anyremote@mail.ru>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA. 
//

package ar.forum;

import android.util.Log;
import android.widget.AbsListView;

public class ItemScrollListener implements AbsListView.OnScrollListener {

    private int firstVisibleItem = -1;
    private int visibleItemCount = -1;
    private int totalItemCount   = -1;
 
    public ItemScrollListener() {
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        //Log.i("ItemScrollListener","onScroll "+firstVisibleItem+" "+visibleItemCount+" "+totalItemCount);
        this.firstVisibleItem = firstVisibleItem;
        this.visibleItemCount = visibleItemCount;
        this.totalItemCount   = totalItemCount;
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        // Don't take any action on changed
    }
    
    public boolean isAtStart() {
         return (firstVisibleItem == 0);
    }
    
    public boolean isAtEnd() {
        return (firstVisibleItem + visibleItemCount == totalItemCount);
    }
}