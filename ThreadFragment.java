//
//Copyright (C) 2016-2017 Mikhail Fedotov <anyremote@mail.ru>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA. 
//

package ar.forum;

import java.util.ArrayList;
import java.util.Vector;

import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import ar.forum.utils.ForumThreadData;
import ar.forum.utils.Prefs;
import ar.forum.utils.ResourceProvider;

public class ThreadFragment extends BaseFragment 
                            implements OnItemClickListener {

    ListView m_list = null;
    ThreadAdapter m_dataSource = null;
    String title;
    private Parcelable m_listSavedState = null;
    boolean haveMultiplePages = false;


    public void setMultiplePage(boolean v) {
        Log.i("ThreadFragment","setMultiplePage "+v);
        haveMultiplePages = v;
     }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        
        Log.i("ThreadFragment","onCreateView");
        DataCollector.setStage(BaseProvider.SHOW_THREAD);
        
        View rootView = inflater.inflate(DataCollector.resourceId(ResourceProvider.LAYOUT_THREADS), container, false);  
        
        m_list = (ListView) rootView.findViewById(DataCollector.resourceId(ResourceProvider.THREAD_LIST));
        m_list.setBackgroundResource(DataCollector.colorBkgr());
        m_list.setDivider(getResources().getDrawable(DataCollector.colorDivider()));
        m_list.setDividerHeight(1);

        onCreateBaseView(m_list); // set scroll counters

        m_list.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        //m_list.setOnItemSelectedListener(this);
        m_list.setOnItemClickListener(this);

        createAdapter();

        return rootView;
    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        Log.i("ThreadFragment", "onSaveInstanceState");
        super.onSaveInstanceState(outState);

        if (m_list != null) {
            m_listSavedState = m_list.onSaveInstanceState();
            outState.putParcelable("thread_state", m_listSavedState);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        Log.i("ThreadFragment", "onActivityCreated");
        if (savedInstanceState != null) {

            Log.i("ThreadFragment", "onActivityCreated have saved state");
            //probably orientation change
            createAdapter();
            m_listSavedState = savedInstanceState.getParcelable("thread_state");

        } else if (m_dataSource != null) {

            //returning from backstack, data is fine, do nothing

         } else {
            //newly created, compute data
            createAdapter();
        }

        if (m_list != null) {
            Log.i("ThreadFragment", "onActivityCreated onRestoreInstanceState");
            if (m_listSavedState != null) {
               m_list.onRestoreInstanceState(m_listSavedState);
           }
           //    registerForContextMenu(m_list);
        }
        //setHasOptionsMenu(true);
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        Log.i("ThreadFragment", "onStart");
        super.onStart();
        onResumeFragment();
    }

    @Override
    public void onResumeFragment() {
        Log.i("ThreadFragment", "onResumeFragment");
        synchronized (DataCollector.provider()) {
            setTitle(title.isEmpty() ? DataCollector.provider().name() : title);
        }
    }

    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
        final ForumThreadData item = m_dataSource.get(arg2);
        if (item != null && !item.href.isEmpty()) {
            DataCollector.sendGlobal(BaseProvider.LOAD_POST, item);
        }
    }   

    /*
    @Override
    public void onCreateContextMenu (ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        Log.i("ThreadFragment","onCreateContextMenu"); 
             
        if (v.getId() == DataCollector.resourceId(ResourceProvider.THREAD_LIST)) {
            
            menu.clear();
            MenuInflater inflater = getActivity().getMenuInflater();
             
            if (DataCollector.isLogged()) {
                inflater.inflate(DataCollector.resourceId(ResourceProvider.THREAD_CTX_MENU_EX), menu);
            } else {
                inflater.inflate(DataCollector.resourceId(ResourceProvider.THREAD_CTX_MENU), menu);
            }
        }
    }

    // Handle context menu, opened by long-click
    @Override
    public boolean onContextItemSelected(MenuItem item) {
     
        final AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();      
        int idx = info.position; 
        
        Log.i("ThreadFragment","onContextItemSelected "+idx); 
         
        int mid = item.getItemId();

        if (mid == DataCollector.resourceId(ResourceProvider.RELOAD)) {
            DataCollector.sendGlobal(BaseProvider.LOAD_THREAD, DataCollector.cache().currentThread());
        } else if (mid == DataCollector.resourceId(ResourceProvider.POST_SEND)) {
               if (idx >= 0) { 
                   final ForumThreadData ft = m_dataSource.get(idx);
                   if (ft != null) {
                       DataCollector.sendGlobal(BaseProvider.PREPARE_POST, ft);
                   }
              }
        }
        return super.onContextItemSelected(item);
    }*/
    
    public BaseAdapter adapter() {
        return m_dataSource;
    }

    private void createAdapter() {
        ArrayList<ForumThreadData> m_listItems = new ArrayList<ForumThreadData>();

        synchronized (DataCollector.provider()) {
            Vector<ForumThreadData> ff = DataCollector.provider().threadData();
            if (!ff.isEmpty()) {
                title = ff.get(0).description;
            }
            title = ff.get(0).description;
            for (int i = 0; i < ff.size(); i++) {
                m_listItems.add(ff.get(i));
            }
        }

        Log.i("ThreadFragment", "createAdapter #" + m_listItems.size());
        m_dataSource = new ThreadAdapter(getActivity(),
                DataCollector.resourceId(ResourceProvider.THREAD_LIST_ITEM_EX), m_listItems);

        if (m_list != null) {
            m_list.setAdapter(m_dataSource);
        }
    }

    public void update() {
        m_dataSource.update();
    };

    public void onZoom(int zoom) {   
        onZoom(zoom, Prefs.POST_TEXT);
    }

    public void onOverFling(int dir) {
        Log.i("ThreadFragment","onOverFling "+dir);
        if (dir == BaseFragment.SWIPE_DOWN) {
            Log.i("ThreadFragment","NEED UPDATE");
            DataCollector.sendGlobal(BaseProvider.LOAD_THREAD_AGAIN, DataCollector.cache().currentThread());
        } else if (dir == BaseFragment.SWIPE_UP) {

            if (haveMultiplePages) {
                Log.i("ThreadFragment", "NEED NEXT PORTION");
                DataCollector.sendGlobal(BaseProvider.LOAD_THREAD_NEXT);
            }
        }
    }
}
