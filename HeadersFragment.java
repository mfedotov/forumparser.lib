//
 //Copyright (C) 2016 Mikhail Fedotov <anyremote@mail.ru>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA. 
//

package ar.forum;

import java.util.ArrayList;

import android.os.Bundle;

import android.os.Parcelable;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import ar.forum.utils.ContentItem;
import ar.forum.utils.ContentProvider;
import ar.forum.utils.Prefs;
import ar.forum.utils.ResourceProvider;

public class HeadersFragment extends BaseFragment 
                             implements OnItemClickListener //,
                                        //AdapterView.OnItemSelectedListener 
{
    ListView       m_list = null;
    HeadersAdapter m_dataSource = null;
    private Parcelable m_listSavedState = null;
    //int selected = -1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Log.i("HeadersFragment", "onCreateView");
        DataCollector.setStage(BaseProvider.SHOW_FORUMS);

        int id = DataCollector.resourceId(ResourceProvider.LAYOUT_HEADERS);
        if (id < 0) {
            Log.i("HeadersFragment", "onCreateView error on resource access");
            return null;
        }

        View rootView = inflater.inflate(id, container, false);

        m_list = (ListView) rootView.findViewById(DataCollector.resourceId(ResourceProvider.HEADER_LIST));
        m_list.setBackgroundResource(DataCollector.colorBkgr());
        m_list.setDivider(getResources().getDrawable(DataCollector.colorDivider()));
        m_list.setDividerHeight(1);
        m_list.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        //m_list.setOnItemSelectedListener(this);
        m_list.setOnItemClickListener(this);

        onCreateBaseView(m_list);

        createAdapter();

        return rootView;
    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        Log.i("HeadersFragment", "onSaveInstanceState");
        super.onSaveInstanceState(outState);

        if (m_list != null) {
            m_listSavedState = m_list.onSaveInstanceState();
            outState.putParcelable("headers_state", m_listSavedState);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        Log.i("HeadersFragment", "onActivityCreated");

        if (savedInstanceState != null) {

            Log.i("HeadersFragment", "onActivityCreated have saved state");

            //probably orientation change;
            createAdapter();
            m_listSavedState = savedInstanceState.getParcelable("headers_state");

        } else if (m_dataSource != null) {

            Log.i("HeadersFragment", "onActivityCreated no saved state but have data");
            //returning from backstack, data is fine, do nothing

         } else {
            //newly created, compute data
            Log.i("HeadersFragment", "onActivityCreated no saved state, create data");
            createAdapter();
        }

        if (m_list != null) {
            if (m_listSavedState != null) {
                 Log.i("HeadersFragment", "onActivityCreated restore state");
                m_list.onRestoreInstanceState(m_listSavedState);
            }
            registerForContextMenu(m_list);
        }
        setHasOptionsMenu(true);
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        Log.i("HeadersFragment", "onStart");
        super.onStart();
        onResumeFragment();
    }

    @Override
    public void onResumeFragment() {
        Log.i("HeadersFragment", "onResumeFragment");
        String v = "";
        synchronized (DataCollector.provider()) {
            ContentProvider p = DataCollector.provider();
            if (p != null) {
                v = p.name();
            }
        }
        setTitle(v);
    }

    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

        Log.i("HeadersFragment", "onItemClick");

        final ContentItem item = m_dataSource.get(arg2);
        if (item == null) {
            return;
        }

        synchronized (DataCollector.cache()) {
            ContentItem ci = m_dataSource.get(arg2);
            if (!ci.href.isEmpty()) {
                DataCollector.sendGlobal(BaseProvider.LOAD_FORUM, ci.idx);
            } else {  // collapse/expand
                m_dataSource.expandOrCollapse(ci.title, arg2);
            }
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        Log.i("HeadersFragment", "onCreateContextMenu");

        int rid = v.getId();

        if (rid == DataCollector.resourceId(ResourceProvider.HEADER_LIST)) {

            DataCollector.setMenuByFragmentId(getID());

            menu.clear();

            final AdapterContextMenuInfo info = (AdapterContextMenuInfo) menuInfo;
            int idx = info.position;

            ContentItem ci = m_dataSource.get(idx);
            if (ci == null) {
                return;
            }
            //Log.i("HeadersFragment", "onCreateContextMenu BBB "+ci.level);

            if (ci.level > 0 || !ci.href.isEmpty()) {   // not a pure header

                final TextView tv = (TextView) info.targetView.findViewById(DataCollector.resourceId(ResourceProvider.HEADER_TEXT));
                menu.setHeaderTitle(tv.getText());

                MenuInflater inflater = getActivity().getMenuInflater();

                if (DataCollector.provider().shortcutsOnly()) {
                    inflater.inflate(DataCollector.resourceId(ResourceProvider.HEADER_CTX_MENU2), menu);  // shortcuts
                } else {
                    inflater.inflate(DataCollector.resourceId(ResourceProvider.HEADER_CTX_MENU), menu);   // favorites
                }
            }
        }
    }

    // Handle context menu, opened by long-click
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (DataCollector.menuByFragmentId() != getID()) {
            return false;
        }

        final AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();

        int idx = info.position;
        Log.i("HeadersFragment","onContextItemSelected "+info.position+" "+item.getItemId());

        int mid = item.getItemId();

        if (mid == DataCollector.resourceId(ResourceProvider.ADD_FAVS)) {
            if (idx >= 0) {
                ContentItem ci = m_dataSource.get(idx);
                if (ci != null) {
                    DataCollector.cache().addFavorite(ci.idx);
                }
            }
            return true;
        } else if (mid == DataCollector.resourceId(ResourceProvider.ADD_SHORTCUT)) {
            if (idx >= 0) {
                ContentItem ci = m_dataSource.get(idx);
                if (ci != null) {
                    DataCollector.cache().addShortcut(ci.title, ci.href, ci.iconUrl, BaseProvider.LOAD_FORUM, true);
                }
            }
            return true;
        } else if (mid == DataCollector.resourceId(ResourceProvider.RELOAD)) {
            DataCollector.sendGlobal(BaseProvider.LOAD_FORUMS);
            return true;
        }

        return super.onContextItemSelected(item);
    }

    private void createAdapter() {
        ArrayList<ContentItem> listItems = new ArrayList<ContentItem>();
        synchronized (DataCollector.cache())

        {
            boolean expanded = false;
            for (int i = 0; i < DataCollector.cache().forumsSize(); i++) {
                ContentItem it = DataCollector.cache().forum(i);
                boolean header = (it.level == 0);
                if (header || expanded || it.expanded) {
                    if (header) {
                        expanded = it.expanded;
                    }
                    listItems.add(it);
                }
            }
        }

        m_dataSource = new HeadersAdapter(getActivity(),
                                          DataCollector.resourceId(ResourceProvider.HEADER_LIST_ITEM), listItems);
        if (m_list != null) {
            m_list.setAdapter(m_dataSource);
        }
    }

    public BaseAdapter adapter() {
        return m_dataSource;
    }
    
    public void onZoom(int zoom) {   
        onZoom(zoom, Prefs.HEADER_TEXT);
    }

    public void onOverFling(int dir) {
        //Log.i("HeadersFragment", "onOverFling " + dir);
        if (dir == BaseFragment.SWIPE_DOWN) {
              Log.i("HeadersFragment", "NEED UPDATE");
              DataCollector.sendGlobal(BaseProvider.LOAD_FORUMS);
        }
     }

    /*@Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
        Log.i("HeadersFragment","onItemSelected #"+arg2);
        selected = arg2;
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
         Log.i("HeadersFragment","onNothingSelected");
         selected = -1;
    }*/
}
