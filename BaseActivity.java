//
// Copyright (C) 2016-2017 Mikhail Fedotov <anyremote@mail.ru>
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA. 
//

package ar.forum;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.graphics.Point;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.Toast;

import ar.forum.utils.Author;
import ar.forum.utils.ContentItem;
import ar.forum.utils.ForumThreadData;
import ar.forum.utils.Prefs;
import ar.forum.utils.ResourceProvider;
import ar.forum.utils.StateMessage;

public class BaseActivity extends    FragmentActivity 
                          implements OnMenuItemClickListener {

    static final int SWIPE_MIN_DISTANCE = 120;
    static final int SWIPE_THRESHOLD_VELOCITY = 200;

    protected static Handler globalHandler = null;

    protected GestureDetector mGestureScanner;
    protected ScaleGestureDetector mScaleGestureDetector;

    protected CharSequence mTitle;

    private float scale = 1;
    public long backPressedTime = 0;

    private int mIdWaitString = -1;
    private int mIdContentFrame = -1;

    private boolean mDownloading = false;

    public BaseActivity() {
    }

    public BaseActivity(int idws, int idcf) {
        mIdWaitString = idws;
        mIdContentFrame = idcf;
    }

    public void initStaticData() {

        setupImageDownloader();

        initGlobalHandler();
        DataCollector.registerHandler(globalHandler);
    }

    @Override
    protected void onPause() {
        Log.i("BaseActivity", "onPause");

        DataCollector.setStop(true);
        //DataCollector.deregisterHandler();
        globalHandler.removeCallbacksAndMessages(null);

        //FragmentManager fm = getSupportFragmentManager();
        //while (fm.getBackStackEntryCount() > 0) {
        //    fm.popBackStackImmediate();
        //}
        super.onPause();
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();

        int stage = DataCollector.stage();
        Log.i("BaseActivity", "onResumeFragments " + stage);

        //initGlobalHandler();
        //DataCollector.registerHandler(globalHandler);
        //if (globalHandler == null) {
        //    Log.i("BaseActivity", "onResumeFragments globalHandler is null");
        //} else {
        //    globalHandler.removeCallbacksAndMessages(null);
        //}

        DataCollector.setStop(false);
        setProgressBarIndeterminateVisibility(false);

        int data = DataCollector.stageData();

        if (stage > BaseProvider.CODE_COMMON_MAX) {
            Log.i("BaseActivity", "onResumeFragments process in child");
            return; // do processing in child class
        }

        /*switch (stage) {
            case BaseProvider.SHOW_FAVS:
                showFavs(false);
                return;
            case BaseProvider.SHOW_SHORTCUTS:
                showShortcuts(false);
                return;
            case BaseProvider.SHOW_FORUMS:
                showHeaders();
                return;
            case BaseProvider.SHOW_FORUM:
                showForum(data);
                return;
            case BaseProvider.SHOW_THREAD:
                showThread();
                return;
            case BaseProvider.SHOW_USER_INFO:
                showUser(data);
                return;
            case BaseProvider.SHOW_SETUP:
                showSetup();
                return;
            case BaseProvider.SHOW_MSGS:
                showMessages();
                return;
            case BaseProvider.SHOW_MSG:
                showMessage();
                return;
            default:
                break;
        }

        String user = DataCollector.prefs().getUser();
        String pass = DataCollector.prefs().getPassword();
        if (!user.isEmpty() && !pass.isEmpty()) {
            DataCollector.sendGlobal(BaseProvider.LOGIN);
        } else {
            startShow();
        }*/
    }

    /*@Override
    protected void onResume() {
        Log.i("BaseActivity", "onResume");
        super.onResume();
    }
    */
    @Override
    protected void onStop() {
        Log.i("BaseActivity", "onStop");
        globalHandler = null;
        super.onStop();
    }
    /*
    @Override
    protected void onDestroy() {
        Log.i("BaseActivity", "onDestroy");
        super.onDestroy();
    }
    */

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        Log.i("BaseActivity", "onSaveInstanceState");
        outState.putString("WORKAROUND_FOR_BUG_19917_KEY", "WORKAROUND_FOR_BUG_19917_VALUE");
        super.onSaveInstanceState(outState);
     }

    /*
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.i("BaseActivity", "onRestoreInstanceState");
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        Log.i("BaseActivity","onAttachFragment "+fragment.getId());
        super.onAttachFragment(fragment);
    }*/

    @Override
    public void onBackPressed() {
        Log.i("BaseActivity","onBackPressed");
        Fragment fh = (Fragment) getSupportFragmentManager().findFragmentByTag(DataCollector.HEADERS_FRAGMENT);
        Fragment ff = (Fragment) getSupportFragmentManager().findFragmentByTag(DataCollector.FAVS_FRAGMENT);
        Fragment sf = (Fragment) getSupportFragmentManager().findFragmentByTag(DataCollector.SHORTCUTS_FRAGMENT);
        //Fragment df = (Fragment) getSupportFragmentManager().findFragmentByTag(DataCollector.FEED_FRAGMENT);

        int depth = getSupportFragmentManager().getBackStackEntryCount();
        //for( int i=0;i<depth;i++) {
        //    Log.i("BaseActivity","onBackPressed stack item "+i+" "+getSupportFragmentManager().getBackStackEntryAt(i).getName());
        //}

        Log.i("BaseActivity","onBackPressed depth #"+depth);
        if (fh != null && fh.isVisible() && depth == 0 ||
                ff != null && ff.isVisible() && depth == 0 ||
                sf != null && sf.isVisible() && depth == 0 /*||
                df != null && df.isVisible() && depth == 0*/) {

            long t = System.currentTimeMillis();
            if (t - backPressedTime > 2000) {

                backPressedTime = t;
                Toast.makeText(this, DataCollector.resourceId(ResourceProvider.PRESS_TO_EXIT), Toast.LENGTH_SHORT).show();
                return;
            }
        }

        //List<Fragment> fragmentList = getSupportFragmentManager().getFragments();
        //for (Fragment frag : fragmentList )  {
        //     getSupportFragmentManager().beginTransaction().remove(frag).commit();
        //}

        super.onBackPressed();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        //Log.i("BaseActivity","dispatchTouchEvent"); 
        mScaleGestureDetector.onTouchEvent(event);
        mGestureScanner.onTouchEvent(event);

        return super.dispatchTouchEvent(event);
    }

    public void addOnBackStackChangedListener() {

        getSupportFragmentManager().addOnBackStackChangedListener(
            new FragmentManager.OnBackStackChangedListener() {
                public void onBackStackChanged() {
                    Log.i("BaseActivity", "onBackStackChanged");

                    int depth = getSupportFragmentManager().getBackStackEntryCount();

                    Log.i("BaseActivity","onBackStackChanged #"+depth);
                    //BaseFragment f = (BaseFragment) getVisibleFragment();
                    //if (f!=null){
                    //    Log.i("MainActivity", "onBackStackChanged FID=" + f.getID());
                    //}

                    int bsIndex = getSupportFragmentManager().getBackStackEntryCount() - 1;
                    if (bsIndex >= 0) {
                        FragmentManager.BackStackEntry fbse = getSupportFragmentManager().getBackStackEntryAt(bsIndex);
                        if (fbse != null) {
                            String name = fbse.getName();
                            Log.i("BaseActivity", "onBackStackChanged " + name);

                            //if (name.startsWith("article")) {
                            //    setTitle(name.substring(7));
                            //}

                            String ttl = DataCollector.getTitle(name);
                            if (ttl != null) {
                                setTitle(ttl);
                            } else {
                                setTitle("");
                            }
                        }
                    } else {
                        setTitle("");
                    }

                    if (depth == 0) {
                        Log.i("BaseActivity","onBackPressed onBackStackChanged");
                        //List<Fragment> fragmentList = getSupportFragmentManager().getFragments();
                        //for (Fragment frag : fragmentList )  {
                        //     getSupportFragmentManager().beginTransaction().remove(frag).commit();
                        //}
                        finish();
                    }

                    /*
                    FragmentManager manager = getSupportFragmentManager();
                    if (manager != null) {
                        int backStackEntryCount = manager.getBackStackEntryCount();
                        if (backStackEntryCount == 0) {
                            finish();
                        } else {
                            BaseFragment fragment = (BaseFragment) manager.getFragments().get(backStackEntryCount - 1);
                            fragment.onResumeFragment();
                        }
                    }
                    */

                }
            });
    }

    @Override
    public void setTitle(CharSequence title) {
        //Log.i("BaseActivity", "setTitle " + title.toString());
        mTitle = title;
        if (getActionBar() != null) {
            getActionBar().setTitle(mTitle);
        }
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        int id = item.getItemId();

        if (id == DataCollector.resourceId(ResourceProvider.USER_PROFILE)) {
            loadCurrentUser();
            return true;
        }
        if (id == DataCollector.resourceId(ResourceProvider.USER_MESSAGES)) {
            loadMessages();
            return true;
        }
        return false;
    }

    // ---------------------------------------------------------------------------------------------------------------
    // Async tasks
    // ---------------------------------------------------------------------------------------------------------------
    public class ThreadPerTaskExecutor implements Executor {
        public void execute(Runnable r) {
            new Thread(r).start();
        }
    }

    public void doTask(Runnable r, boolean lock) {

        setProgressBarIndeterminateVisibility(true);
        if (lock) {
            mDownloading = true;
        }

        Toast.makeText(this, mIdWaitString, Toast.LENGTH_SHORT).show();

        Executor executor = new ThreadPerTaskExecutor();
        executor.execute(r);
    }

    public void login() {
        String url = DataCollector.provider().loginURL();
        Log.i("BaseActivity", "login " + (url == null ? "NULL" : url));
        if (url != null) {
            doTask(new LoginTask(url), false);
        }
    }

    // ---------------------------------------------------------------------------------------------------------------
    // Load data from web
    // ---------------------------------------------------------------------------------------------------------------

    public void readWebpage(String url, int stage, int data) {
        Log.i("BaseActivity", "readWebpage " + data + " " + (url == null ? "NULL" : url));
        if (mDownloading) {
            Log.i("BaseActivity", "readWebpage skip request, already have one");
            return;
        }
        if (url != null) {
            doTask(new DownloadTask(url, stage, data), true);
        }
    }

    public void loadForumList() {
        Log.i("BaseActivity", "loadForumList");
        DataCollector.prefs().setForumListSynced(false);
        DataCollector.cache().clearForums();
        showSplash();
        readWebpage(DataCollector.provider().topForumsURL(), BaseProvider.LOAD_FORUMS, -1);
    }

    public void loadForum(int fidx, int operation) {

        String href = "";
        synchronized (DataCollector.cache()) {

            DataCollector.cache().resetForum();

            ContentItem it = DataCollector.cache().forum(fidx);
            if (it != null) {
                href = it.href;
            }
        }
        if (!href.isEmpty()) {
            readWebpage(DataCollector.provider().forumURL(href, 1), operation, fidx);
        }
    }

    public void loadForumPage(int fidx) {

        String href = "";
        synchronized (DataCollector.cache()) {

            //int data = DataCollector.stageData();
            int pidx = DataCollector.cache().incrPage();

            Log.i("BaseActivity", "loadForumPage #" + fidx + " " + pidx);

            ContentItem it = DataCollector.cache().forum(fidx);
            if (it != null) {
                href = DataCollector.provider().forumURL(it.href, pidx);
            }
        }
        if (!href.isEmpty()) {
            Log.i("BaseActivity", "loadForumPage " + href);
            readWebpage(href, BaseProvider.LOAD_FORUM_NEXT, fidx);
        } else {
            Log.i("BaseActivity", "loadForumPage     empty URL for page" + fidx);
        }
    }

    public void loadThread(String href, int operation) {

        if (!href.isEmpty()) {
            synchronized (DataCollector.cache()) {
                DataCollector.cache().resetThread();
                DataCollector.cache().setCurrentThread(href);
            }
            readWebpage(DataCollector.provider().threadURL(href, 1), operation, -1);
        }
    }

    public void loadThreadPage() {
        synchronized (DataCollector.cache()) {
            String href = DataCollector.cache().currentThread();
            if (!href.isEmpty()) {
                int page = DataCollector.cache().incrThreadPage();
                ;
                readWebpage(DataCollector.provider().threadURL(href, page), BaseProvider.LOAD_THREAD_NEXT, -1);
            }
        }
    }

    public void loadPost(ForumThreadData ftd) {
        //Log.i("BaseActivity", "loadPost " + ftd.href + " " + ftd.html + " " + ftd.description);
    }

    public void showPost(String href) {
        //Log.i("BaseActivity", "showPost " + href);
    }

    public void handleStateMessage(Message msg) {
         Log.i("BaseActivity", "handleStateMessage");
    }

    public void loadUser(int v) {
        Log.i("BaseActivity", "loadUser #" + v);
        Author a = DataCollector.authors().author(v);
        if (a != null) {
            Log.i("BaseActivity", "loadUser >" + a.href + "<");
            if (a.html == null || a.html.isEmpty()) {
                if (!a.href.isEmpty()) {
                    readWebpage(a.href, BaseProvider.LOAD_USER_INFO, -1);
                } else {
                    showDownloadError();
                }
            } else {
                showUser(v);
            }
        }
    }

    public void loadCurrentUser() {
        Log.i("BaseActivity", "loadCurrentUser");
        readWebpage(DataCollector.provider().profileURL(""), BaseProvider.CURUSER_INFO, -1);
    }

    public void loadMessages() {
        Log.i("BaseActivity", "loadMessages");
        readWebpage(DataCollector.provider().messagesURL(), BaseProvider.LOAD_MSGS, -1);
    }

    public void loadMessage(int i) {
        Log.i("BaseActivity", "loadMessage " + i);
        ForumThreadData item = DataCollector.provider().msg(i);
        if (item != null) {
            readWebpage(DataCollector.provider().messageURL(item.href), BaseProvider.LOAD_MSG, -1);
        }
    }

    public void loadUserMessages(int i, int operation) {
        Log.i("BaseActivity", "loadUserMessages " + i);
        DataCollector.cache().resetUserPage();

        Author a = null;
        if (i == Authors.CURUSER_ID) {
            a = DataCollector.authors().author();
        } else {
            a = DataCollector.authors().author(i);
        }
        if (a != null) {
            Log.i("BaseActivity", "loadUserMessages " + a.name + " id=" + a.id + " " + a.href);
            readWebpage(DataCollector.provider().authorMessagesURL(a, 0), operation, i);
        }
    }

    public void loadNextUserMessages(int i) {
        int pidx = -1;
        synchronized (DataCollector.cache()) {
            pidx = DataCollector.cache().incrUserPage();
        }
        Author a = DataCollector.authors().author(i);
        if (a != null) {
            Log.i("BaseActivity", "loadNextUserMessages #" + pidx);
            readWebpage(DataCollector.provider().authorMessagesURL(a, pidx), BaseProvider.LOAD_USER_MSGS_NEXT, i);
        }
    }

    // ---------------------------------------------------------------------------------------------------------------
    // Send data
    // ---------------------------------------------------------------------------------------------------------------


    // ---------------------------------------------------------------------------------------------------------------
    // Show fragments
    // ---------------------------------------------------------------------------------------------------------------

    public void showPopup(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        MenuInflater inflater = popup.getMenuInflater();
        popup.setOnMenuItemClickListener(this);
        inflater.inflate(DataCollector.resourceId(ResourceProvider.USER_MENU), popup.getMenu());
        popup.show();
    }

    public void showSearchDialog() {
        SearchFragment f = new SearchFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager != null) {
            f.show(fragmentManager,"search");
        }
    }

    public void showSplash() {
        Log.i("BaseActivity", "showSplash");
        SplashFragment f = new SplashFragment();
        f.setup(DataCollector.colorBkgrSplash());
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager != null) {
            fragmentManager.beginTransaction().replace(mIdContentFrame, f).commitAllowingStateLoss();
        }
    }

    public void showAbout() {
        Fragment fragment = new About();
        FragmentManager fragmentManager = getSupportFragmentManager();
        DataCollector.addTitle("about", getResources().getString(DataCollector.resourceId(ResourceProvider.APP_NAME)));
        fragmentManager.beginTransaction().add(mIdContentFrame, fragment).addToBackStack("about").commitAllowingStateLoss();
    }

    public void startShow() {
        if (DataCollector.cache().forumsEmpty()) {
            Log.i("BaseActivity", "startShow load forum list from internet");
            showSplash();
            DataCollector.sendGlobal(BaseProvider.LOAD_FORUMS);
        } else {
            Log.i("BaseActivity", "startShow use cached forum list");
            if (DataCollector.prefs().startWithFavs()) {
                showFavs(false);
            } else {
                showHeaders();
            }
        }
    }

    public void showHeaders() {
        Log.i("BaseActivity", "showHeaders");
        Fragment fragment = new HeadersFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().add(mIdContentFrame, fragment, DataCollector.HEADERS_FRAGMENT).commit(); //AllowingStateLoss();
    }

    public void showFavs(boolean addToStack) {
        Fragment fragment = new FavoritesFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction().add(mIdContentFrame, fragment, DataCollector.FAVS_FRAGMENT);
        if (addToStack) {
            DataCollector.addTitle("favs", getResources().getString(DataCollector.resourceId(ResourceProvider.FAVS)));
            ft.addToBackStack("favs");
        }
        ft.commit(); //AllowingStateLoss();
    }

    public void showShortcuts(boolean addToStack) {
        Fragment fragment = new ShortcutFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();


        FragmentTransaction ft = fragmentManager.beginTransaction().add(mIdContentFrame, fragment, DataCollector.SHORTCUTS_FRAGMENT);
        if (addToStack) {
            DataCollector.addTitle("shortcuts", getResources().getString(DataCollector.resourceId(ResourceProvider.SHORTCUTS)));
            ft.addToBackStack("shortcuts");
        }
        ft.commit(); //AllowingStateLoss();
    }

    public void showForum(int id) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        ForumFragment f = new ForumFragment();
        f.setForum(id);
        DataCollector.addTitle("threads", "");
        fragmentManager.beginTransaction().add(mIdContentFrame, f).addToBackStack("threads").commit(); //commitAllowingStateLoss();
    }

    public void showThread() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        ThreadFragment f = new ThreadFragment();
        f.setMultiplePage(DataCollector.provider().isThreadMultiPaged());
        DataCollector.addTitle("onethread", "");
        fragmentManager.beginTransaction().add(mIdContentFrame, f).addToBackStack("onethread").commit(); //commitAllowingStateLoss();
    }

    public void loadSearch(String data) {
    }

    public void loadShortcut(int type, String href) {
        Log.i("BaseActivity:", "loadShortcut #" + type);
        if (type < BaseProvider.CODE_COMMON_MAX) {
             if (type == BaseProvider.LOAD_FORUM) {
                  // get index by href
                  int idx = DataCollector.cache().forumIndex(href);
                  if (idx >= 0) {
                      //DataCollector.sendGlobal(type, idx);
                      loadForum(idx, BaseProvider.LOAD_FORUM);
                  }
             }
            if (type == BaseProvider.LOAD_THREAD) {
                //DataCollector.sendGlobal(type, href);
                loadThread(href, BaseProvider.LOAD_THREAD);
            }
        }
    }

    public void showUser(int v) {
        Log.i("BaseActivity:", "showUser #" + v);
        FragmentManager fragmentManager = getSupportFragmentManager();
        UserFragment f = new UserFragment();
        f.setup(v);
        fragmentManager.beginTransaction().add(mIdContentFrame, f).addToBackStack("user").commit(); //AllowingStateLoss();
    }

    public void showMessages() {
        Log.i("BaseActivity", "showMessages");
        FragmentManager fragmentManager = getSupportFragmentManager();
        MsgsFragment f = new MsgsFragment();
        f.setType(ForumAdapter.ADAPT_MSGLIST);
        fragmentManager.beginTransaction().add(mIdContentFrame, f).addToBackStack("msgs").commit(); //AllowingStateLoss();
    }

    public void showMessage() {
        Log.i("BaseActivity", "showMessage");
        FragmentManager fragmentManager = getSupportFragmentManager();
        MsgsFragment f = new MsgsFragment();
        f.setType(ForumAdapter.ADAPT_MSGTHREAD);
        fragmentManager.beginTransaction().add(mIdContentFrame, f).addToBackStack("msg").commit(); //AllowingStateLoss();
    }

    public void showUserMessages(int uid) {
        Log.i("BaseActivity", "showUserMessages " + uid);
        FragmentManager fragmentManager = getSupportFragmentManager();
        UserPostsFragment f = new UserPostsFragment();
        f.setup(uid);
        fragmentManager.beginTransaction().add(mIdContentFrame, f).addToBackStack("usermsgs").commitAllowingStateLoss();
    }

    public void showNewMessage(ForumThreadData fd, int uid) {
        if (fd == null && uid < 0) {
            return;
        }
        Log.i("BaseActivity", "showMessage MSG/POST");
        NewMsgFragment f = new NewMsgFragment();
        f.setup(fd, uid);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().add(mIdContentFrame, f).addToBackStack("newmsg").commitAllowingStateLoss();
    }

    // ---------------------------------------------------------------------------------------------------------------
    // Preferences
    // ---------------------------------------------------------------------------------------------------------------

    public void saveStartWithFavs(int v) {
        DataCollector.prefs().saveStartWithFavs(this, v);
    }

    public void saveTheme(int v) {
        DataCollector.prefs().saveTheme(this, v);
    }

    public void saveTextSize(int type, int v) {
        DataCollector.prefs().saveTextSize(this, type, v);
    }

    public void saveUser(String u, String p) {
        Log.i("BaseActivity", "saveUser " + u + " " + p);
        if (DataCollector.prefs().saveUser(this, u, p)) {
            Log.i("BaseActivity", "saveUser try to login");
            login();
        }
    }

    public void saveFavs() {
        DataCollector.prefs().saveFavs(this);
    }

    public void saveShortcuts() {
        DataCollector.prefs().saveShortcuts(this);
    }

    public void savePrefs() {
        DataCollector.prefs().savePrefs(this);
    }

    public void deletePrefs() {
        // TODO: delete all other stuff
        DataCollector.prefs().deletePrefs(this);
    }

    // ---------------------------------------------------------------------------------------------------------------
    // Fragments
    // ---------------------------------------------------------------------------------------------------------------

    public Fragment getVisibleFragment() {
        FragmentManager fragmentManager = (FragmentManager) getSupportFragmentManager();
        List<Fragment> fragments = new ArrayList<Fragment>();
        fragments = fragmentManager.getFragments();
        Fragment vis = null;
        for (Fragment fragment : fragments) {
            if (fragment != null && fragment.isVisible()) {
                vis = fragment; // do not break !!!
            }
        }
        return vis;
    }

    public void sendToFragment(int dir) {
        Log.i("BaseActivity","sendToFragment "+dir);
        BaseFragment f = (BaseFragment) getVisibleFragment();
        if (f != null) {
            Log.i("BaseActivity","sendToFragment id="+f.getID());
            f.onGest(dir);
        }
    }

    // ---------------------------------------------------------------------------------------------------------------
    // Gestures
    // ---------------------------------------------------------------------------------------------------------------
    public void setupGestures() {

        mGestureScanner = new GestureDetector(this,

                new GestureDetector.SimpleOnGestureListener() {
                    @Override
                    public boolean onDown(MotionEvent e) {
                        return true;
                    }

                    @Override
                    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                        // Log.i("BaseActivity","onFling "+velocityX+" "+velocityY);
                        try {
                            // right to left swipe
                            // if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE &&
                            // Math.abs(velocityX) >SWIPE_THRESHOLD_VELOCITY) {
                            // //Log.i("BaseActivity","onFling SlideLeft");
                            // } else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE &&
                            // Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                            // //Log.i("BaseActivity","onFling SlideRight");
                            // } else

                            if (e1.getY() - e2.getY() > SWIPE_MIN_DISTANCE && Math.abs(velocityY) > SWIPE_THRESHOLD_VELOCITY) {
                                // Log.i("BaseActivity","onFling SlideUp");
                                sendToFragment(BaseFragment.SWIPE_UP);
                            } else if (e2.getY() - e1.getY() > SWIPE_MIN_DISTANCE
                                    && Math.abs(velocityY) > SWIPE_THRESHOLD_VELOCITY) {
                                // Log.i("BaseActivity","onFling SlideDown");
                                sendToFragment(BaseFragment.SWIPE_DOWN);
                            }
                        } catch (Exception e) {
                            // nothing
                        }

                        return true;
                    }
                });

        mScaleGestureDetector = new ScaleGestureDetector(this, new ScaleGestureDetector.OnScaleGestureListener() {
            @Override
            public boolean onScale(ScaleGestureDetector d) {

                float ratio = scale / d.getCurrentSpan();
                if (ratio < 1.1 && ratio > 0.9) {
                    return true;
                }
                scale = d.getCurrentSpan();
                int type = (ratio < 1 ? BaseFragment.SWIPE_ZOOMIN : BaseFragment.SWIPE_ZOOMOUT);
                sendToFragment(type);
                return true;
            }

            @Override
            public boolean onScaleBegin(ScaleGestureDetector d) {
                scale = d.getCurrentSpan();
                return true;
            }

            @Override
            public void onScaleEnd(ScaleGestureDetector d) {
                scale = 1;
            }
        });
    }

    // ---------------------------------------------------------------------------------------------------------------
    // FSM
    // ---------------------------------------------------------------------------------------------------------------

    public void initGlobalHandler() {

        Log.i("BaseActivity", "initGlobalHandler");
        if (globalHandler != null) {
            Log.i("BaseActivity", "initGlobalHandler exists");
            return;
        }

        globalHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {

               // Log.i("handleMessage", "ENTER");

                setProgressBarIndeterminateVisibility(false);

                FragmentManager fragmentManager = getSupportFragmentManager();
                if (fragmentManager == null) {
                    Log.i("BaseActivity", "handleMessage no FragmentManager");
                    return;
                }

                if (msg.what > BaseProvider.CODE_COMMON_MAX) {  // do processing in child classes
                    handleStateMessage(msg);
                    return;
                }
                switch (msg.what) {

                    //case SHOW_SPLASH:
                    //
                    //     Log.i("handleMessage","SHOW_SPLASH");
                    //     showSplash();
                    //     break;

                    case BaseProvider.LOGIN:

                        Log.i("BaseActivity", "handleMessage LOGIN");
                        showSplash();
                        login();
                        startShow();
                        break;

                    case BaseProvider.LOGIN_OK:

                        Log.i("BaseActivity", "handleMessage LOGIN_OK");
                        DataCollector.setLogged(true);
                        invalidateOptionsMenu();
                        break;

                    case BaseProvider.LOGIN_ERR:

                        Log.i("BaseActivity", "handleMessage LOGIN_ERR");
                        DataCollector.setLogged(false);
                        invalidateOptionsMenu();
                        showLoginError((String) msg.obj);
                        break;

                    case BaseProvider.SHOW_FAVS:

                        Log.i("BaseActivity", "handleMessage SHOW_FAVS");
                        showFavs(false);
                        break;

                    case BaseProvider.UPDATE_VISIBLE:

                        Log.i("BaseActivity", "handleMessage UPDATE_VISIBLE");

                        BaseFragment fr = (BaseFragment) getVisibleFragment();
                        if (fr != null) {
                            fr.update();
                        }
                        break;

                    case BaseProvider.LOAD_FORUMS:

                        Log.i("BaseActivity", "handleMessage LOAD_FORUMS");
                        loadForumList();
                        break;

                    case BaseProvider.SHOW_FORUMS:

                        Log.i("BaseActivity", "handleMessage SHOW_FORUMS");

                        if (!DataCollector.prefs().isForumListSynced()) {
                            Log.i("handleMessage", "savePrefs");
                            savePrefs();
                        }

                        showHeaders();
                        break;

                    case BaseProvider.LOAD_FORUM:

                        Log.i("BaseActivity", "handleMessage LOAD_FORUM");
                        loadForum(((Integer) msg.obj).intValue(), BaseProvider.LOAD_FORUM);
                        break;

                    case BaseProvider.LOAD_FORUM_AGAIN:

                        Log.i("BaseActivity", "handleMessage LOAD_FORUM_AGAIN");
                        loadForum(((Integer) msg.obj).intValue(), BaseProvider.LOAD_FORUM_AGAIN);
                        break;

                    case BaseProvider.LOAD_FORUM_NEXT:

                        Log.i("BaseActivity", "handleMessage LOAD_FORUM_NEXT");
                        loadForumPage(((Integer) msg.obj).intValue());
                        break;

                    case BaseProvider.SHOW_FORUM:

                        Log.i("BaseActivity", "handleMessage SHOW_FORUM");
                        showForum(((Integer) msg.obj).intValue());
                        break;

                    case BaseProvider.LOAD_THREAD:

                        Log.i("BaseActivity", "handleMessage LOAD_THREAD");
                        loadThread((String) msg.obj, BaseProvider.LOAD_THREAD);
                        break;

                    case BaseProvider.LOAD_THREAD_AGAIN:

                        Log.i("BaseActivity", "handleMessage LOAD_THREAD_AGAIN");
                        loadThread((String) msg.obj, BaseProvider.LOAD_THREAD_AGAIN);
                        break;

                    case BaseProvider.LOAD_THREAD_NEXT:

                        Log.i("BaseActivity", "handleMessage LOAD_THREAD_NEXT");
                        loadThreadPage();
                        break;

                    case BaseProvider.SHOW_THREAD:

                        Log.i("BaseActivity", "handleMessage SHOW_THREAD");
                        showThread();
                        break;

                    case BaseProvider.LOAD_SEARCH:

                        Log.i("BaseActivity", "handleMessage LOAD_SEARCH");
                        loadSearch((String) msg.obj);
                        break;

                    case BaseProvider.LOAD_SHORTCUT:

                        Log.i("BaseActivity", "handleMessage LOAD_SHORTCUT");
                        StateMessage m = (StateMessage) msg.obj;
                        loadShortcut(m.id, m.str);
                        break;

                    case BaseProvider.LOAD_POST:

                        Log.i("BaseActivity", "handleMessage POST_SELECTED");
                        loadPost((ForumThreadData) msg.obj);
                        break;

                    case BaseProvider.SHOW_POST:

                        Log.i("BaseActivity", "handleMessage POST_READY");
                        showPost((String) msg.obj);
                        break;

                    case BaseProvider.SHOW_WEB:

                        Log.i("BaseActivity", "handleMessage SHOW_WEB");
                        showWebPage((String) msg.obj);
                        break;

                    case BaseProvider.SHOW_WEB_EXTERNAL:

                        Log.i("BaseActivity", "handleMessage SHOW_WEB_EXTERNAL");
                        showWebExternal((String) msg.obj);
                        break;

                    case BaseProvider.LOAD_USER_INFO:

                        Log.i("BaseActivity", "handleMessage LOAD_USER_INFO");
                        loadUser(((Integer) msg.obj).intValue());
                        break;

                    case BaseProvider.SHOW_USER_INFO:

                        Log.i("BaseActivity", "handleMessage SHOW_USER_INFO");
                        showUser(((Integer) msg.obj).intValue());
                        break;

                    case BaseProvider.CURUSER_READY:

                        Log.i("BaseActivity", "handleMessage CURUSER_READY");
                        showUser(Authors.CURUSER_ID);
                        break;

                    case BaseProvider.LOAD_MSGS:

                        Log.i("BaseActivity", "handleMessage LOAD_MSGS");
                        loadMessages();
                        break;

                    case BaseProvider.SHOW_MSGS:

                        Log.i("BaseActivity", "handleMessage SHOW_MSGS");
                        showMessages();
                        break;

                    case BaseProvider.LOAD_MSG:

                        Log.i("BaseActivity", "handleMessage LOAD_MSG");
                        loadMessage(((Integer) msg.obj).intValue());
                        break;

                    case BaseProvider.SHOW_MSG:

                        Log.i("BaseActivity", "handleMessage SHOW_MSG");
                        showMessage();
                        break;

                    case BaseProvider.LOAD_USER_MSGS:

                        Log.i("BaseActivity", "handleMessage LOAD_USER_MSGS");
                        loadUserMessages(((Integer) msg.obj).intValue(), BaseProvider.LOAD_USER_MSGS);
                        break;

                    case BaseProvider.LOAD_USER_MSGS_AGAIN:

                        Log.i("BaseActivity", "handleMessage LOAD_USER_MSGS_AGAIN");
                        loadUserMessages(((Integer) msg.obj).intValue(), BaseProvider.LOAD_USER_MSGS_AGAIN);
                        break;

                    case BaseProvider.LOAD_USER_MSGS_NEXT:

                        Log.i("BaseActivity", "handleMessage LOAD_USER_MSGS_NEXT");
                        loadNextUserMessages(((Integer) msg.obj).intValue());
                        break;

                    case BaseProvider.SHOW_USER_MSGS:

                        Log.i("BaseActivity", "handleMessage SHOW_USER_MSGS");
                        showUserMessages(((Integer) msg.obj).intValue());
                        break;

                    case BaseProvider.PREPARE_MESSAGE:

                        Log.i("BaseActivity", "handleMessage PREPARE_MESSAGE");
                        showNewMessage(null, ((Integer) msg.obj).intValue());
                        break;

                    case BaseProvider.SEND_MESSAGE:

                        ForumThreadData d = ((ForumThreadData) msg.obj);
                        Log.i("BaseActivity", "handleMessage SEND_MESSAGE");
                        sendNewMessage(d);
                        break;

                    case BaseProvider.SEND_MESSAGE_ERR:

                        Log.i("BaseActivity", "handleMessage SEND_MESSAGE_ERR");
                        showNewMsgError((String) msg.obj);
                        break;

                    case BaseProvider.PREPARE_POST:

                        Log.i("BaseActivity", "handleMessage PREPARE_POST");
                        showNewMessage(((ForumThreadData) msg.obj), -1);
                        break;

                    case BaseProvider.SEND_POST:

                        ForumThreadData dp = ((ForumThreadData) msg.obj);
                        Log.i("BaseActivity", "handleMessage SEND_POST " + dp.href + " " + dp.description);
                        sendNewPost(dp);
                        break;

                    case BaseProvider.SEND_POST_ERR:

                        Log.i("BaseActivity", "handleMessage SEND_POST_ERR");
                        showNewMsgError((String) msg.obj);
                        break;

                    case BaseProvider.SETUP_USER:

                        Log.i("BaseActivity", "handleMessage SETUP_USER");
                        String[] up = ((String) msg.obj).split("\n");
                        if (up.length >= 2) {
                            saveUser(up[0], up[1]);
                        }
                        break;

                    case BaseProvider.SETUP_OPENFAVS:

                        Log.i("BaseActivity", "handleMessage SETUP_OPENFAVS");
                        saveStartWithFavs(((Integer) msg.obj).intValue());
                        break;

                    case BaseProvider.SETUP_THEME:

                        Log.i("BaseActivity", "handleMessage SETUP_THEME");
                        saveTheme(((Integer) msg.obj).intValue());
                        break;

                    case BaseProvider.SETUP_HEADER_SIZE:

                        Log.i("BaseActivity", "handleMessage SETUP_HEADER_SIZE");
                        saveTextSize(Prefs.HEADER_TEXT, ((Integer) msg.obj).intValue());
                        break;

                    case BaseProvider.SETUP_POST_SIZE:

                        Log.i("BaseActivity", "handleMessage SETUP_POST_SIZE");
                        saveTextSize(Prefs.POST_TEXT, ((Integer) msg.obj).intValue());
                        break;

                    case BaseProvider.SAVE_SHORTCUTS:

                        Log.i("BaseActivity", "handleMessage SAVE_SHORTCUTS");
                        saveShortcuts();
                        break;

                    case BaseProvider.SAVE_FAVS:

                        Log.i("BaseActivity", "handleMessage SAVE_FAVS");
                        saveFavs();
                        break;

                    case BaseProvider.DELETE_ALL:

                        Log.i("BaseActivity", "handleMessage DELETE_ALL");
                        deletePrefs();
                        break;

                    case BaseProvider.DLOAD_RES:

                        int rc = ((Integer) msg.obj).intValue();
                        Log.i("BaseActivity", "handleMessage DLOAD_RES " + rc);

                        mDownloading = false;

                        if (rc == Downloader.LOAD_ERR) {
                            showDownloadError();
                        } else if (rc == Downloader.LOGOUT) {
                            DataCollector.setLogged(false);
                            invalidateOptionsMenu();
                        }
                        break;
                }

                int depth = getSupportFragmentManager().getBackStackEntryCount();
                Log.i("BaseActivity","handleMessage fragment stack #"+depth);
            }
        };
    }

    // Derived classes can decide to open HTML page inside app
    public void showWebPage(String url) {
        Log.i("BaseActivity", "showWebPage");
        //showWebExternal(url);
        WebFragment fragment = new WebFragment();
        fragment.setup(url, true);

        FragmentManager fragmentManager = getSupportFragmentManager();

        String site = url.replace("http://","").replace("https://","");
        int slash = site.indexOf('/');
        site = site.substring(0,slash);

        DataCollector.addTitle(site, getResources().getString(DataCollector.resourceId(ResourceProvider.APP_NAME)));
        fragmentManager.beginTransaction().add(mIdContentFrame, fragment).addToBackStack("www").commitAllowingStateLoss();
    }

    public void showWebExternal(String url) {
        Log.i("BaseActivity", "showWebExternal");
        try {
            Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivityForResult(webIntent, 1);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i("BaseActivity", "onActivityResult");
    }

    // ---------------------------------------------------------------------------------------------------------------
    // Utils
    // ---------------------------------------------------------------------------------------------------------------
    
    public void sendNewMessage(ForumThreadData d) {  // override in child class
    }
    
    public void sendNewPost(ForumThreadData d) {  // override in child class
    }
    
    public void showSetup()  {        // override in child class
    }
    
    public void showDownloadError() { 
        Toast.makeText(this, DataCollector.resourceId(ResourceProvider.DLOAD_ERR), Toast.LENGTH_SHORT).show();       
    }
    
    public void showLoginError(String err) {
        
        if (err.compareTo(DataCollector.WRONG_PASS) == 0) {
            err = getResources().getString(DataCollector.resourceId(ResourceProvider.WRONG_PASS));
        }
        
        String msg = getResources().getString(DataCollector.resourceId(ResourceProvider.LOGIN_ERR))+" : "+err;
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();       
    }
  
    public void showNewMsgError(String err) {
        String msg = getResources().getString(DataCollector.resourceId(ResourceProvider.NEWMSG_ERR))+" : "+err;
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();       
    }
    
    public void setupImageDownloader() {
        
        String cacheDir = "";
        try {
            cacheDir = getExternalCacheDir().getCanonicalPath();
        } catch (Exception e1) {
            Log.i("BaseActivity","there is no ExternalCacheDir available");
            try {
                cacheDir = getCacheDir().getCanonicalPath();
            } catch (Exception e2) {
                Log.i("BaseActivity","there is no CacheDir available");
            }
        }
        
        DataCollector.registerImageDownloader(cacheDir);
    }        
    
    public void setupScreenParam() {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        DataCollector.setScreenParam(size.x, size.y);
    }
}
