//
//Copyright (C) 2016 Mikhail Fedotov <anyremote@mail.ru>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA. 
//

package ar.forum;

import java.util.HashMap;
import java.util.Map;

import ar.forum.utils.StateMessage;
import httpimage.BitmapCache;
import httpimage.FileSystemPersistence;
import httpimage.HttpImageManager;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import ar.forum.utils.ContentProvider;
import ar.forum.utils.CookieManager;
import ar.forum.utils.ForumThreadData;
import ar.forum.utils.Prefs;
import ar.forum.utils.ResourceProvider;
import ar.forum.utils.ThemeProvider;

public class DataCollector {

    public static final String HEADERS_FRAGMENT   = "HEADERS";
    public static final String FAVS_FRAGMENT      = "FAVS";
    public static final String SHORTCUTS_FRAGMENT = "SHORTCUTS";
    //public static final String FEED_FRAGMENT      = "FEED";

    public static final String WRONG_PASS         = "_WRONG_PASS_";
   
    private static DataCache     cache     = new DataCache();
    private static Prefs         prefs     = new Prefs();
    private static CookieManager cookies   = new CookieManager();

    static Map<String,String> titles    = new HashMap<String,String>();

    private static boolean       loggedIn  = false;
    private static int           sw        = 0;
    private static int           sh        = 0;
    private static int           stage     = BaseProvider.SHOW_SPLASH;
    private static int           stageData = -1;

    private static int           menuByFragmentId = -1;
    
    private static boolean       stopped   = true;
    
    private static Handler          globalHandler = null;
    private static ContentProvider  provider      = null;
    private static Authors          authors       = null;
    private static ThemeProvider    theme         = null;
    private static ResourceProvider resources     = null; 
    private static HttpImageManager imageManager  = null;
    
    public static void init() {
        //cookies = new java.net.CookieManager();
        //cookies.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        //CookieHandler.setDefault(cookies);
    }
    
    public static void registerHandler (Handler h) {
        globalHandler = h;
    }  

    public static void deregisterHandler () {
        globalHandler = null;
    }  
   
    public static void setStop(boolean v) {
        stopped   = v;
    }

    public static boolean isStopped() {
        return stopped;
    } 
    
    public static void setStage(int id) {
        if (!stopped) {
            stage     = id;
            stageData = -1;
        }
    }

    public static void setStage(int id, int data) {
        stage     = id;
        stageData = data;
    }

    public static int stage() {
        return stage;
    }
 
    public static int stageData() {
        return stageData;
    }

    public static void setMenuByFragmentId(int id) {menuByFragmentId = id;}

    public static int menuByFragmentId() {return menuByFragmentId;}
    
    public static void sendGlobal(int id) {
        if (globalHandler != null) {
            Log.i("DataCollector","sendGlobal #"+id);
            globalHandler.sendEmptyMessage(id);
        }
    }

    public static void sendGlobal(int id, int data) {
        if (globalHandler != null) {
            Log.i("DataCollector","sendGlobal #"+id);
            Message msg = globalHandler.obtainMessage(id, new Integer(data));
            msg.sendToTarget();
        }
    }
    
    public static void sendGlobal(int id, String data) {
        if (globalHandler != null) {
            Log.i("DataCollector","sendGlobal #"+id);
            Message msg = globalHandler.obtainMessage(id, new String(data));
            msg.sendToTarget();
        }
    }
    
    public static void sendGlobal(int id, ForumThreadData data) {
        if (globalHandler != null) {
            Log.i("DataCollector","sendGlobal #"+id);
            Message msg = globalHandler.obtainMessage(id, ((Object) data));
            msg.sendToTarget();
        }
    }

    public static void sendGlobal(int id, StateMessage data) {
        if (globalHandler != null) {
            Log.i("DataCollector","sendGlobal #"+id);
            Message msg = globalHandler.obtainMessage(id, ((Object) data));
            msg.sendToTarget();
        }
    }
    public static DataCache cache() {
        return cache;
    }
    
    public static Prefs prefs() {
        return prefs;
    }
    
    public static CookieManager cookies() {
        return cookies;
    } 
    
    public static void registerImageDownloader(String cacheDir) {
        if (imageManager == null) {
             imageManager = new HttpImageManager(HttpImageManager.createDefaultMemoryCache(), 
                                                (BitmapCache) new FileSystemPersistence(cacheDir));
        }
    }

    public static Bitmap loadImageWithNotif(String url) {

        Bitmap bm = imageManager.loadImage(new HttpImageManager.LoadRequest(Uri.parse(url),
                new HttpImageManager.OnLoadResponseListener() {

                    public void onLoadResponse(HttpImageManager.LoadRequest r,
                                               Bitmap data) {
                        DataCollector.sendGlobal(BaseProvider.UPDATE_VISIBLE, 0);
                    }

                    public void onLoadProgress(HttpImageManager.LoadRequest r,
                                               long totalContentSize,
                                               long loadedContentSize) {
                    }

                    public void onLoadError(HttpImageManager.LoadRequest r,
                                            Throwable e) {
                    }
                }));
        return bm;
    }

    public static HttpImageManager imageDownloader() {
        return imageManager;
    }
 
    public static void registerProvider(ContentProvider h) {
        provider = h;
        if (authors == null) {
            authors = new Authors();            
        }
    }    

    public static void registerTheme(ThemeProvider h) {
        theme = h;
    }  
    
    public static void registerResources(ResourceProvider h) {
        resources = h;      
    }
    
    public static ContentProvider provider() {
        return provider;
    }
 
    public static Authors authors() {
        if (authors == null) {
            authors = new Authors();            
        }
        return authors;
    }
   
    public static int colorBkgr() {
        return (theme == null ? -1 : theme.colorBkgr(prefs.getTheme()));
    }

    public static int colorBkgrSplash() {
        return (theme == null ? -1 : theme.colorBkgrSplash(prefs.getTheme()));
    }

    public static int colorItemBkgr() {
        return (theme == null ? -1 : theme.colorItemBkgr(prefs.getTheme()));
    }

    public static int colorDivider() {
       return (theme == null ? -1 : theme.colorDivider(prefs.getTheme()));
    }

    public static int colorFrame() {
        return (theme == null ? -1 : theme.colorFrame(prefs.getTheme()));
    }
 
    public static int colorText() {
        return (theme == null ? -1 : theme.colorText(prefs.getTheme()));
    }

    public static int colorTextGrayed() {
        return (theme == null ? -1 : theme.colorTextGrayed(prefs.getTheme()));
    }

    public static int resourceId(int id) {
        return (resources == null ? -1 : resources.resourceId(id));
    }
    
    public static void setLogged(boolean v) {
        loggedIn = v;
    }
    
    public static boolean isLogged() {
        return loggedIn;
    }
    
    public static void setScreenParam(int w, int h) {
        sw = w;
        sh = h;
    }
    
    public static int screenWidth() {
        return sw;
    }

    public static int screenSize() {  // min(w,h)
        return (sw > sh ? sh : sw);
    }

    // TODO: cleanup ?
    public static void addTitle(String key, String v) {
        titles.put(key,v);
    }

    public static String getTitle(String key) {
        return titles.get(key);
    }
}
