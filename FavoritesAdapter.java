//
//Copyright (C) 2016 Mikhail Fedotov <anyremote@mail.ru>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA. 
//

package ar.forum;

import java.util.ArrayList;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import ar.forum.utils.FavoriteItem;
import ar.forum.utils.Prefs;
import ar.forum.utils.ResourceProvider;

public class FavoritesAdapter extends BaseAdapter<FavoriteItem> {
    
    public FavoritesAdapter(Context context, int textViewResourceId, ArrayList<FavoriteItem> items) {
        super(context, textViewResourceId, items);
        this.context = context;
        this.items   = items;   
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        
        final View v;
        
        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(DataCollector.resourceId(ResourceProvider.HEADER_LIST_ITEM), null);
        } else {
            v = convertView;
        }

        TextView  txt = (TextView) v.findViewById(DataCollector.resourceId(ResourceProvider.HEADER_TEXT));
        int ts = (int) txt.getTextSize();
        int textSize = DataCollector.prefs().textSize(Prefs.HEADER_TEXT);
        if (textSize < 0) { 
            //Log.i("FavoritesAdapter","getView store text size "+ts);
            DataCollector.prefs().saveTextSize(null,Prefs.HEADER_TEXT, ts);
            DataCollector.sendGlobal(BaseProvider.SETUP_HEADER_SIZE, textSize);  // will save text size value again
        } else if (textSize != ts) {
            //Log.i("FavoritesAdapter","getView set text size "+textSize);
            txt.setTextSize(textSize);
        }

        ImageView ic  = (ImageView) v.findViewById(DataCollector.resourceId(ResourceProvider.EXPAND_ICON));
        ic.setVisibility(View.GONE);
        
        txt.setTextColor(this.context.getResources().getColor(DataCollector.colorText()));
        
        synchronized (items) {
            FavoriteItem item = items.get(position);
            if (item != null) {
                if (!item.title.isEmpty()) {
                     txt.setText(item.title);
                     txt.setGravity(Gravity.RIGHT);                 
                }
            }
        }
        return v;
    }
}
