//
//Copyright (C) 2016 Mikhail Fedotov <anyremote@mail.ru>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA. 
//

package ar.forum;

import java.util.ArrayList;
import java.util.Vector;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import ar.forum.utils.Author;
import ar.forum.utils.ForumThreadData;
import ar.forum.utils.Prefs;
import ar.forum.utils.ResourceProvider;

public class ForumAdapter extends BaseAdapter<ForumThreadData> implements OnClickListener {

	public static final int ADAPT_FORUM     = 0;
	public static final int ADAPT_MSGLIST   = 1;
	public static final int ADAPT_MSGTHREAD = 2;
    public static final int ADAPT_USERMSGS  = 3;

	int     type;

    public ForumAdapter(Context context, int textViewResourceId, int type, ArrayList<ForumThreadData> items) {
        super(context, textViewResourceId, items);
        Log.i("ForumAdapter","ForumAdapter #"+type);
        this.type = type;
    }

    public void update() {

        items.clear();

        synchronized (DataCollector.provider()) {
            Vector<ForumThreadData> ff = (type == ADAPT_FORUM ?
                                           DataCollector.provider().forumData() :
            		                    	   (type == ADAPT_MSGLIST ?
            		                    	    DataCollector.provider().msgList() :
            		                    	        (type == ADAPT_USERMSGS ?
            		                    	                DataCollector.provider().userMessages() :
            		                    	                DataCollector.provider().msgThread()
            		                    	        )
            		                    	   )
            		                     );
            for (int i=0;i<ff.size();i++) {
                items.add(ff.get(i));
            }
        }

        this.notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //Log.i("ForumAdapter","getView #"+position);

        final View v;

        int layoutId = (type == ADAPT_USERMSGS ? ResourceProvider.UPOST_LIST_ITEM : ResourceProvider.THREAD_LIST_ITEM);

        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(DataCollector.resourceId(layoutId), null);
        } else {
            v = convertView;
        }

        LinearLayout row = (LinearLayout) v.findViewById(DataCollector.resourceId(ResourceProvider.THREAD_ROW));
        row.setBackgroundResource(DataCollector.colorBkgr());

        LinearLayout rounded = (LinearLayout) v.findViewById(DataCollector.resourceId(ResourceProvider.THREAD_ITEM));
        rounded.setBackgroundResource(DataCollector.colorFrame());

        boolean resetSize = false;
        TextView txt  = (TextView) v.findViewById(DataCollector.resourceId(ResourceProvider.THREAD_TITLE));
        int ts = (int) txt.getTextSize();
        int textSize = DataCollector.prefs().textSize(Prefs.POST_TEXT);
        if (textSize < 0) {
            DataCollector.prefs().saveTextSize(null,Prefs.POST_TEXT,ts);
            DataCollector.sendGlobal(BaseProvider.SETUP_POST_SIZE, textSize);  // will save text size value again
        } else if (textSize != ts) {
            txt.setTextSize(textSize);
            resetSize = true;
        }
        txt.setTextColor(this.context.getResources().getColor(DataCollector.colorText()));

        TextView forumHeader = (type == ADAPT_USERMSGS ?
                                (TextView) v.findViewById(DataCollector.resourceId(ResourceProvider.UPOST_ITEM)) :
                                null);
        if (forumHeader != null) {
            forumHeader.setTextColor(this.context.getResources().getColor(DataCollector.colorText()));
        }

        TextView user = (TextView) v.findViewById(DataCollector.resourceId(ResourceProvider.USER));
        user.setOnClickListener(this);

        TextView date = (TextView) v.findViewById(DataCollector.resourceId(ResourceProvider.DATE));
        TextView answ = (type == ADAPT_USERMSGS ? null :
                         (TextView) v.findViewById(DataCollector.resourceId(ResourceProvider.ANSWERS)));

        user.setTextColor(this.context.getResources().getColor(DataCollector.colorTextGrayed()));
        date.setTextColor(this.context.getResources().getColor(DataCollector.colorTextGrayed()));
        if (answ != null) {
            answ.setTextColor(this.context.getResources().getColor(DataCollector.colorTextGrayed()));
        }

        if (resetSize) {
            user.setTextSize(textSize-1);  // a bit smaller than post text
            date.setTextSize(textSize-1);

            if (answ != null) {
                 answ.setTextSize(textSize-1);
            }

            if (forumHeader != null) {
                forumHeader.setTextSize(textSize-1);
            }
        }

        synchronized (items) {
            ForumThreadData item = items.get(position);
            if (item != null && !item.description.isEmpty()) {

                txt.setText(item.description);

                if (type == ADAPT_USERMSGS && forumHeader != null) {
                    forumHeader.setText(item.html);
                }

                if (answ != null) {
                    if (type == ADAPT_FORUM) {
                        answ.setText(" + " + String.valueOf(item.answers));
                    } else if (type == ADAPT_MSGLIST) {
                        answ.setText(String.valueOf(item.answers) + " (" + String.valueOf(item.level) + ")");
                    } else {
                        answ.setText("");
                    }
                }
                date.setText(item.date);

                Author a = DataCollector.authors().author(item.authorIndex);
                if (a == null) {
                	if (type == ADAPT_MSGTHREAD) {
                        user.setText(DataCollector.resourceId(ResourceProvider.CUR_USER));
                    } else if (type != ADAPT_FORUM) {
                		user.setText(item.html);
                	}
                } else {
                    user.setText(a.name);

                    ImageView icView = (ImageView) v.findViewById(DataCollector.resourceId(ResourceProvider.USER_ICON));
                    icView.setImageResource(DataCollector.resourceId(ResourceProvider.USER_DRAW_ICON));

                    if (a.icon != null && !a.icon.isEmpty()) {

                        String ic = a.icon;
                        if (ic.startsWith("//")) {
                            ic = "http:"+ic;
                        }

                        //Log.i("ForumAdapter","getView ICON LOAD "+ic);
                        Bitmap bm = DataCollector.loadImageWithNotif(ic);

                        if (bm != null) {
                        	icView.setImageBitmap(bm);
                        }
                    }
                }
            }
        }

        return v;
    }

    @Override
    public void onClick(View arg0) {
        Log.i("ForumAdapter", "onClick " + type + " " + ((TextView) arg0).getText().toString());

        int idx = DataCollector.authors().authorIdxByName(((TextView) arg0).getText().toString());

        if (idx >= 0) {
            //if (type == ADAPT_FORUM || type == ADAPT_MSGTHREAD || type == ADAPT_USERMSGS) {
                DataCollector.sendGlobal(BaseProvider.LOAD_USER_INFO, idx);
            //} else if (type == ADAPT_MSGLIST) {
            //    DataCollector.sendGlobal(DataCollector.MSG_INFO, idx);
            //}
        }
    }
}
