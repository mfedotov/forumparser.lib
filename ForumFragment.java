//
//Copyright (C) 2016-2017 Mikhail Fedotov <anyremote@mail.ru>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA. 
//

package ar.forum;

import java.util.ArrayList;
import java.util.Vector;

import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import ar.forum.utils.ForumThreadData;
import ar.forum.utils.Prefs;
import ar.forum.utils.ResourceProvider;

public class ForumFragment extends BaseFragment 
                           implements OnItemClickListener {
    
    private ListView     m_list = null;
    private ForumAdapter m_dataSource = null;
    private Parcelable m_listSavedState = null;
    private int          m_forumId = -1;
    
    public ForumFragment() {
    }

    public void setForum(int id) {
         this.m_forumId = id;
     }
 
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        
        Log.i("ForumFragment","onCreateView");

        DataCollector.setStage(BaseProvider.SHOW_FORUM, m_forumId);
       
        View rootView = inflater.inflate(DataCollector.resourceId(ResourceProvider.LAYOUT_THREADS), container, false); 
        
        m_list = (ListView) rootView.findViewById(DataCollector.resourceId(ResourceProvider.THREAD_LIST));
        m_list.setBackgroundResource(DataCollector.colorBkgr());
        m_list.setDivider(getResources().getDrawable(DataCollector.colorBkgr()));
        m_list.setDividerHeight(3);

        onCreateBaseView(m_list); // set scroll counters

        m_list.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        //m_list.setOnItemSelectedListener(this);
        m_list.setOnItemClickListener(this);

        createAdapter();

        return rootView;
    }
    
    @Override
    public void onSaveInstanceState(final Bundle outState) {
        Log.i("ForumFragment", "onSaveInstanceState");
        super.onSaveInstanceState(outState);

        if (m_list != null) {
            m_listSavedState = m_list.onSaveInstanceState();
            outState.putParcelable("forum_state", m_listSavedState);
        }
    }
    
    @Override
    public void onActivityCreated (Bundle savedInstanceState) {
        Log.i("ForumFragment","onActivityCreated");

        if (savedInstanceState != null) {

            Log.i("ForumFragment","onActivityCreated have saved state");
            //probably orientation change
            createAdapter();
            m_listSavedState = savedInstanceState.getParcelable("forum_state");

        } else if (m_dataSource != null) {

            //returning from backstack, data is fine, do nothing

        } else {
            //newly created, compute data
            createAdapter();
        }

        if (m_list != null) {

            if (m_listSavedState != null) {
                 Log.i("ForumFragment","onActivityCreated onRestoreInstanceState");
                 m_list.onRestoreInstanceState(m_listSavedState);
            }
            registerForContextMenu(m_list);
        }
        setHasOptionsMenu(true);
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        Log.i("ForumFragment", "onStart");
        super.onStart();
        onResumeFragment();
    }

    @Override
    public void onResumeFragment() {
        Log.i("ForumFragment", "onResumeFragment");
        setTitle(DataCollector.cache().forumTitle(m_forumId));
    }
   
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
        final ForumThreadData item = m_dataSource.get(arg2);
        if (item != null && !item.href.isEmpty()) {
            DataCollector.sendGlobal(BaseProvider.LOAD_THREAD, item.href);
        }
    }
    
    @Override
    public void onCreateContextMenu (ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        Log.i("ForumFragment","onCreateContextMenu"); 

        int mid = v.getId();

        if (mid == DataCollector.resourceId(ResourceProvider.THREAD_LIST)) {

            DataCollector.setMenuByFragmentId(getID());

            menu.clear();
          
            final AdapterContextMenuInfo info = (AdapterContextMenuInfo)menuInfo;
            int idx = info.position; 
            
            ForumThreadData ci = m_dataSource.get(idx);
            if (ci == null) {
                return;
            }
            
            final TextView tv = (TextView) info.targetView.findViewById(DataCollector.resourceId(ResourceProvider.THREAD_TITLE));
            if (tv != null) {
                menu.setHeaderTitle(tv.getText());
            }
           
            MenuInflater inflater = getActivity().getMenuInflater();

            if (DataCollector.isLogged()) {
                inflater.inflate(DataCollector.resourceId(ResourceProvider.FORUM_CTX_MENU_EX), menu);
            } else {
                inflater.inflate(DataCollector.resourceId(ResourceProvider.FORUM_CTX_MENU), menu);
            }
        }
    }

    // Handle context menu, opened by long-click
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (DataCollector.menuByFragmentId() != getID()) {
            return false;
        }

        if( getUserVisibleHint() == false ) {
            return false;
        }

        final AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();

        int idx = info.position;
        Log.i("ForumFragment","onContextItemSelected "+info.position+" "+item.getItemId()); 
        
        int mid = item.getItemId();

        if (mid == DataCollector.resourceId(ResourceProvider.ADD_SHORTCUT)) {

            if (idx >= 0) { 
                final ForumThreadData ci = m_dataSource.get(idx);
                if (ci != null) {
                    Log.i("ForumFragment","onContextItemSelected #"+ci.href);
                    DataCollector.cache().addShortcut(ci.description, ci.href, BaseProvider.LOAD_THREAD, true);
                }
            }
            return true;
        } else if (mid == DataCollector.resourceId(ResourceProvider.POST_SEND)) {   
 
              if (idx >= 0) { 
                   final ForumThreadData ft = m_dataSource.get(idx);
                   if (ft != null) {
                       DataCollector.sendGlobal(BaseProvider.PREPARE_POST, ft);
                   }
              }
              return true;
         }
        
        return super.onContextItemSelected(item);
    }
    
    private void createAdapter() {
        
        ArrayList<ForumThreadData> listItems = new ArrayList<ForumThreadData>();
        
        synchronized (DataCollector.provider()) {
            Vector<ForumThreadData> ff = DataCollector.provider().forumData();
            for (int i = 0; i < ff.size(); i++) {
                listItems.add(ff.get(i));
            }
        }

        Log.i("ForumFragment","createAdapter #"+listItems.size());
        m_dataSource = new ForumAdapter(getActivity(),
                        DataCollector.resourceId(ResourceProvider.THREAD_LIST_ITEM),
                        ForumAdapter.ADAPT_FORUM, listItems);
        if (m_list != null) {
            m_list.setAdapter(m_dataSource);
        }
    }
    
    public BaseAdapter adapter() {
        return m_dataSource;
    }

    public void onZoom(int zoom) {   
        onZoom(zoom, Prefs.POST_TEXT);
    }
    
    public void update() {
        Log.i("ForumFragment","update"); 
        //m_dataSource.clear();
        //createAdapter();
        //m_dataSource.notifyDataSetChanged();
        //m_list.setAdapter(m_dataSource);
        m_dataSource.update();
    }
    
    public void onOverFling(int dir) {
        Log.i("ForumFragment","onOverFling "+dir); 
        if (dir == BaseFragment.SWIPE_DOWN) {
            Log.i("ForumFragment","NEED UPDATE"); 
            DataCollector.sendGlobal(BaseProvider.LOAD_FORUM_AGAIN, m_forumId);
        } else if (dir == BaseFragment.SWIPE_UP) {
            Log.i("ForumFragment","NEED NEXT PORTION"); 
            DataCollector.sendGlobal(BaseProvider.LOAD_FORUM_NEXT, m_forumId);
        }
    }
}
