//
//Copyright (C) 2016 Mikhail Fedotov <anyremote@mail.ru>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA. 
//

package ar.forum;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import android.util.Log;

public class Downloader {

    public static final int LOAD_OK    = 0;
    public static final int LOAD_ERR   = 1;
    public static final int LOGOUT     = 2;
    
    static final String COOKIES_HEADER = "Set-Cookie";
    static final String USER_AGENT     = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.36 Safari/537.36";
   
    public static HttpURLConnection createConnectionGET(String url) {
        //Log.i("Downloader", "::createConnectionGET START " + url);
        try {
            URL obj = new URL(url);

            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            
            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", USER_AGENT);

            if (DataCollector.cookies().size() > 0) {
                //Log.i("Downloader", "::createConnectionGET add cookies #" + DataCollector.cookies().size());
                //DataCollector.cookies().dumpCookies();
                con.setRequestProperty("Cookie", DataCollector.cookies().getCookies());    
            }
            
            //CookieStore cookieJar =  DataCollector.cookies().getCookieStore();
            //List <HttpCookie> cookies = cookieJar.getCookies();
            //for (HttpCookie cookie: cookies) {
            //  Log.i("Downloader::createConnectionGET ", "cookie: " + cookie);
            //}
            return con;
        } catch (Exception e) {
            //Log.i("Downloader", "::createConnectionGET Exception");
            e.printStackTrace();
            DataCollector.sendGlobal(BaseProvider.DLOAD_RES, LOAD_ERR);
        }
        //Log.i("Downloader", "::createConnectionGET return null");
        return null;
    }
      
    public static String sendGet(String url, boolean skipData) { 
        
        //Log.i("Downloader::sendGet", url);
        
        StringBuffer response = new StringBuffer();
            
        HttpURLConnection con = createConnectionGET(url);   // can be null
        if (con == null) {
            DataCollector.sendGlobal(BaseProvider.DLOAD_RES, LOAD_ERR);
            return  "";
        }

        try {
            int code = con.getResponseCode();
            DataCollector.cookies().storeCookies(con);

            //Log.i("Downloader::sendGet", "Response Code: " +
            //           (code == HttpURLConnection.HTTP_MOVED_TEMP ? "HTTP_MOVED_TEMP" :
            //            (code == HttpURLConnection.HTTP_MOVED_PERM ? "HTTP_MOVED_PERM" :
            //             (code == HttpURLConnection.HTTP_SEE_OTHER ? "HTTP_SEE_OTHER" : String.valueOf(code))
            //            )
            //           )
            //     );
            
            while ((code == HttpURLConnection.HTTP_MOVED_TEMP || 
                    code == HttpURLConnection.HTTP_MOVED_PERM  || 
                    code == HttpURLConnection.HTTP_SEE_OTHER)) {
                
                String newUrl = con.getHeaderField("Location");
                if (!newUrl.isEmpty() && newUrl.charAt(0) == '/') {
                    newUrl = con.getURL().getProtocol() + "://" + con.getURL().getHost() + newUrl;              
                }
                //Log.i("Downloader::sendGet", "... redirected "+newUrl);
                
                con = createConnectionGET(newUrl);  // can be null
                if (con == null) {
                    //Log.i("Downloader::sendGet", "can not get response");
                    return  "";
                //} else {
                //    Log.i("Downloader::sendGet", "able to get response");
                }
                //Log.i("Downloader::sendGet", "MSG="+con.getResponseMessage());
                code = con.getResponseCode();
                DataCollector.cookies().storeCookies(con);
                //Log.i("Downloader::sendGet", "Redirected response Code: " + code);
            }

            if (!skipData) {
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

                String inputLine;
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
            }
            
        } catch (Exception e) {
            Log.i("Downloader::sendGet", "Exception " + e.getMessage());
            DataCollector.sendGlobal(BaseProvider.DLOAD_RES, LOAD_ERR);
        }
        
        con.disconnect();
        //Log.i("Downloader::sendGet", "---------------------- RESPONSE: -----------------------" + response.toString());
        //Log.i("Downloader::sendGet", "+++------------------- RESPONSE: -----------------------");
        return response.toString();
    }
}
