//
//Copyright (C) 2016 Mikhail Fedotov <anyremote@mail.ru>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA. 
//

package ar.forum;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import ar.forum.utils.ResourceProvider;

public class SplashFragment extends BaseFragment {

    private int bg  = -1;

    public void setup(int v) {
        bg = v;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        
        Log.i("SplashFragment","onCreateView "+bg);

        int layoutId = DataCollector.resourceId(ResourceProvider.LAYOUT_SPLASH);
        if (layoutId == -1) { // seems all data were lost on suspend/resume
            //((BaseActivity) getActivity()).initStaticData();
            return null;
        }

        View rootView = inflater.inflate(layoutId, container, false);
        if (bg != -1) {
            rootView.setBackgroundResource(bg);
        }
      
        return rootView;
    }
}
