//
// Copyright (C) 2016 Mikhail Fedotov <anyremote@mail.ru>
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA. 
//

package ar.forum;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import ar.forum.utils.ResourceProvider;

public class WebFragment extends BaseFragment {

    private String m_url    = "";
    private boolean m_isurl = false;

    public void setup(String v, boolean isUrl) {
        m_url   = v;
        m_isurl = isUrl;
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        
        Log.i("WebFragment","onCreateView "+m_url+" "+m_isurl);
        DataCollector.setStage(BaseProvider.SHOW_WEB);
        
        View rootView = inflater.inflate(DataCollector.resourceId(ResourceProvider.LAYOUT_WEB), container, false);  
  
        WebView page = (WebView)  rootView.findViewById(DataCollector.resourceId(ResourceProvider.VIEW_WEB));
        
        WebSettings webSettings = page.getSettings();
        
        webSettings.setJavaScriptEnabled(false); 
        webSettings.setJavaScriptCanOpenWindowsAutomatically(false);
        webSettings.setSupportMultipleWindows(false);
        webSettings.setSupportZoom(true); 
        webSettings.setBuiltInZoomControls(true);
        webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE); 

        page.setWebChromeClient(new WebChromeClient());
        page.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.i("WebFragment", "shouldOverrideUrlLoading " + url);
                if (url.startsWith("vnd.youtube")) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                } else if (url.startsWith("http")) {
                    DataCollector.sendGlobal(BaseProvider.SHOW_WEB, url);
                }
                return true;
            }
        });

        if (m_isurl) {
            page.loadUrl(m_url);
        } else {
            int sw = DataCollector.screenWidth()-6;

            page.loadData("<style>img{display: inline; height: auto; max-width: 100%;}</style>\n"+m_url,
                          "text/html; charset=" + DataCollector.provider().encoding(), null);
        }
        return rootView;
    } 
   
    @Override
    public void onActivityCreated (Bundle savedInstanceState) {
        Log.i("WebFragment","onActivityCreated");
        super.onActivityCreated(savedInstanceState);
    }
 }
